package belvedererestaurantproject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import org.joda.time.DateTime;

/**
 @note Generate Random data and inserts them to tables                                   
 */
public class RandomGenerator {
	
	static MysqlConnect db = new MysqlConnect();

	public static void main(String[] args) throws IOException, ParseException, SQLException{
		Random rand = new Random();
		
		/** change to true to turn random generator on */
		boolean generator_on = false;
		
		if(generator_on == true){
			/** re-run db_schema - start fresh */
			db.executeSqlScript();
			
			/** set total rows to generate */
			int generateTotal = 4780;
			
			/** set default */
			int waiter_no = 0;
			String curr_datetime;
			
			try{
				/** Establish DB connection */
				MysqlConnect.getDbCon();
				
				/** Get First Shift Employees id */
				String sql_waiters1 = "SELECT employee_id "
									  + "FROM EMPLOYEE "
									 + "WHERE job_type   = 'waiter' "
									   + "AND shift_type = 'first'";
				ResultSet waiters_1 = db.query(sql_waiters1);
				ArrayList<Integer> firstShift = new ArrayList<Integer>();
				while (waiters_1.next()) {
					firstShift.add(waiters_1.getInt("employee_id"));
				}
				
				/** Get Second Shift Employees id */
				String sql_waiters2 = "SELECT employee_id "
						              + "FROM EMPLOYEE "
						             + "WHERE job_type   = 'waiter' "
						               + "AND shift_type = 'second'";
				ResultSet waiters_2 = db.query(sql_waiters2);
				ArrayList<Integer> secondShift = new ArrayList<Integer>();
				while (waiters_2.next()) {
					secondShift.add(waiters_2.getInt("employee_id"));
				}
				
				/** Get Menu Items */
				String sql_menuItems = "SELECT item_no "
						               + "FROM MENU";
				ResultSet menu = db.query(sql_menuItems);
				ArrayList<Integer> menuItems = new ArrayList<Integer>();
				while (menu.next()) {
					menuItems.add(menu.getInt("item_no"));
				}
				
				/** Get txt files - place in array */
			    BufferedReader br1 = new BufferedReader(new FileReader("first_name.txt"));
				ArrayList<String> first_name = new ArrayList<String>();
				String f_name = null;
				while ((f_name = br1.readLine()) != null) {
					first_name.add(f_name);
				}
				br1.close();
			    
			    BufferedReader br2 = new BufferedReader(new FileReader("last_name.txt"));
				ArrayList<String> last_name = new ArrayList<String>();
				String l_name = null;
				while ((l_name = br2.readLine()) != null) {
					last_name.add(l_name);
				}
				br2.close();
			    
				BufferedReader br3 = new BufferedReader(new FileReader("addresses.txt"));
				ArrayList<String> address = new ArrayList<String>();
				String new_add = null;
				while ((new_add = br3.readLine()) != null) {
					address.add(new_add);
				}
				br3.close();
				
				ArrayList<Integer> tableOccupants = new ArrayList<Integer>();
				for(int i = 0; i < generateTotal; i++){
					/** randomize the number of occupants in a table */
					int tableCnt = rand.nextInt(6) + 1;
					tableOccupants.add(tableCnt);
				}
				
				/** Set time formatter and defaults */
				Calendar calendar = Calendar.getInstance();
				calendar.set(2016, 6, 19);
				DateFormat date_timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				DateFormat closing_timeFormat = new SimpleDateFormat("HH:mm:ss");
				DateFormat secondShiftFormat = new SimpleDateFormat("HH:mm:ss");
				
				/** store opening time */
				String opening = "08:00:00";
				java.util.Date opening_time = closing_timeFormat.parse(opening);
				DateTime open = new DateTime(opening_time);
				int opening_hour = open.getHourOfDay();
				
				/** store closing time */
				String closing = "20:00:00";
				java.util.Date closing_time = closing_timeFormat.parse(closing);
				DateTime close = new DateTime(closing_time);
				int closing_hour = close.getHourOfDay();
				
				/** second shift start time */
				String secondShiftTime = "14:00:00";
				java.util.Date secShift = secondShiftFormat.parse(secondShiftTime);
				DateTime sec_shift = new DateTime(secShift);
				int second_shft = sec_shift.getHourOfDay();
				
				/** Bias array used for randomizing orders */
				int [] quantity_bin = {1,1,1,1,1,1,1,1,1,1,1,1,2,3};
				
				String[] paymentType = {"CASH","CARD"};
				
				/** Start Loop and inserts */
				int count = 0; 
			    while (count < tableOccupants.size()) {
			    	
			    	int reserve_seating = rand.nextInt(15) + 1;
			    	
					/** random minutes & seconds */
					int addTime = rand.nextInt(20) + 5;
					
			        /** Add datetime to bill table */
					calendar.add(Calendar.MINUTE, addTime);
					calendar.add(Calendar.SECOND, addTime);
					
					DateTime curr = new DateTime(calendar.getTime()); 
					int curr_hour = curr.getHourOfDay();
							
					if(curr_hour > closing_hour || curr_hour < opening_hour){
						calendar.add(Calendar.HOUR, 11);
						curr_datetime = date_timeFormat.format(calendar.getTime());
						
						/** get waiter from first shift */
						int index = rand.nextInt(firstShift.size());
				        waiter_no = firstShift.get(index);
				        
					}else if(curr_hour > second_shft){
				        int index = rand.nextInt(secondShift.size());
				        waiter_no = secondShift.get(index);
				        curr_datetime = date_timeFormat.format(calendar.getTime());
				        
					}else{
						int index = rand.nextInt(firstShift.size());
				        waiter_no = firstShift.get(index);
				        curr_datetime = date_timeFormat.format(calendar.getTime());
					}
				
			    	/** table no (max 9 tables in restaurant) */
			    	int table_no = rand.nextInt(9) + 1;
			    	
			    	/** add table first to get table_id */
			    	String sql_tbl = "INSERT INTO TABLES "
			    			       + "SET table_id    = NULL,"
			    			           + "table_no    = '"+table_no+"',"
			    			           + "employee_id = '"+waiter_no+"'";
			        int table_id = db.set(sql_tbl);
			    	
			    	/** insert pre-determined amount of customers into current table */
					for(int customer_no = 0; customer_no < tableOccupants.get(count); customer_no++){
											
				        String sql_cust = "INSERT INTO CUSTOMER "
				        		        + "SET cust_id     = NULL,"
				        		            + "c_fname     = NULL,"
				        		            + "c_lname     = NULL,"
				        		            + "address     = NULL,"
				        		            + "city        = NULL,"
				        		            + "state       = NULL," 
				        		            + "postal_code = NULL,"
				        		            + "phone_no    = NULL";
					    /** Query returns Auto Incremented ID of inserted rows */
					    int cust_id = db.set(sql_cust);
					    
					    String sql_cust_table = "INSERT INTO CUST_TABLE "
					    		              + "SET table_id = '"+table_id+"',"
					    		                  + "cust_id  = '"+cust_id+"'";
					    /** Query returns Auto Incremented ID of inserted rows */
					    db.set(sql_cust_table);
					    
					    
						if(reserve_seating == 10 && customer_no == 0){
							int index = rand.nextInt(address.size());
					        String get_address = address.get(index);
					        String[] customer_info = get_address.split(",");
					        
					    	String sql_reserve = "INSERT INTO RESERVATION "
					    			           + "SET reservation_id = NULL,"
					    			               + "phone_no       = '"+customer_info[5]+"',"
					    			               + "date_time      = '"+curr_datetime+"', "
					    			               + "cust_id        = '"+cust_id+"' ";
					        db.set(sql_reserve);
						}
				    }
					
					/** Generate bill */
					String sql_bill = "INSERT INTO BILL "
							        + "SET bill_id   = NULL, "
							            + "date_time = '"+curr_datetime+"', "
							            + "total     = 0";
					int bill_id = db.set(sql_bill);
					
					/** random orders per customer */
			        int orderNeeded = quantity_bin[(int)(Math.random()*quantity_bin.length)];
			        
			        /** order quantity */
			        int quantity = 1;
			        if(orderNeeded < 2){
			        	quantity = quantity_bin[(int)(Math.random()*quantity_bin.length)];
			        }
			        
			        int is_canceled = 0;
			        int canceled_order = rand.nextInt(30) + 1;
			        if(canceled_order == 10){
			        	is_canceled = 1;
			        }
			        
			        /** Insert Customer Order and table connection*/     
					String sql_order = "INSERT INTO ORDERS "
							         + "SET order_id     = NULL, "
							             + "table_id     = '"+table_id+"', "
							             + "bill_id      = '"+bill_id+"', "
							             + "is_active    = 0, " // transaction already happened - no longer active
							             + "is_canceled  = '"+is_canceled+"' ";
			        
			        int order_id = db.set(sql_order);
			        
			        /** Add or subtract a random amount of order to this current tables order */
			        int add_item = rand.nextInt(3)-1;
			        
			        int orderToTake = tableOccupants.get(count)+add_item;
			        
			        /** subtracting too much will result in a bill totaling to 0.00 */
			        if(orderToTake <= 0){
			        	orderToTake += 2;
			        }
			        
			        double total = 0;
				    for(int curr_order = 0; curr_order <= tableOccupants.get(count)+add_item; curr_order++){
				    	
				    	/** get a random menuItem */
					    int items = rand.nextInt(menuItems.size());
				        int item_no = menuItems.get(items);
				        
				        /** get price and total cost of item based on quantity*/
				        String sql_menu = "SELECT * "
				        		          + "FROM MENU "
				        		         + "WHERE item_no = "+item_no+"";
						ResultSet item_price = db.query(sql_menu);
					
						double price1 = 0;
						while (item_price.next()) {
							price1 = item_price.getDouble("price");
							total += item_price.getDouble("price") * quantity;
						}
						
						 /** get item name*/
						String sql_item = "SELECT item_name "
								          + "FROM MENU "
								         + "WHERE item_no = '"+item_no+"' ";
						ResultSet rs2 = db.query(sql_item);
						
						String item_name = null;
						while(rs2.next()){
							item_name = rs2.getString("item_name");
						}
						
				    	/** Insert order */
				        String sql_tblOrder = "INSERT INTO TABLE_ORDERS "
				        		            + "SET order_id  = '"+order_id+"', "
				        		            	+ "item_no   = '"+item_no+"', "
				        		            	+ "item_name = '"+item_name+"', "
				        		            	+ "price     = '"+price1+"',"
				        		            	+ "quantity  = '"+quantity+"'";
				        db.set(sql_tblOrder); 
				    }
				    
				    /** Update total of  bill */
					String sql_bill_update = "UPDATE BILL "
							                  + "SET total   = '"+total+"' "
							                + "WHERE bill_id = '"+bill_id+"' ";
					db.set(sql_bill_update);
					
					if(is_canceled != 1){
						/** randomize between 1-2 payments */
						if(total > 75){
							for(int n = 0; n < paymentType.length; n++){
								/** Get Payment */
								String sql_payment = "INSERT INTO PAYMENT_TYPE "
										           + "SET p_type_id   = NULL, "
										               + "p_type_name = '"+paymentType[n]+"', "
										               + "bill_id     = '"+bill_id+"'";
								db.set(sql_payment);
							}
						}else{
							int idx = rand.nextInt(paymentType.length);
							String p_type = (paymentType[idx]);
							
							/** Get Payment */
							String sql_payment = "INSERT INTO PAYMENT_TYPE "
									           + "SET p_type_id   = NULL, "
									               + "p_type_name = '"+p_type+"', "
									               + "bill_id     = '"+bill_id+"'";
							db.set(sql_payment);
						}
					}
					count++;
					
					System.out.println(count+" of "+generateTotal+" orders created");
			   }
			}catch(SQLException se){
				/** Handle errors for JDBC */
				se.printStackTrace(); 
			}
			
			System.out.println("Done!!!");
		}
	}
}
