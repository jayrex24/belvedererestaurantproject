package belvedererestaurantproject;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;

import net.miginfocom.swing.MigLayout;

/**
 @see Main.java
 @returns active_orders panel
 */
public interface ActiveOrders {
	
	JPanel active_orders = new JPanel();
	MysqlConnect db = new MysqlConnect();
	String pattern = "###,###.###";
	DecimalFormat decimalFormat = new DecimalFormat(pattern);
	
	
	default Component activeOrders() throws SQLException {
				
		/** used to refresh panel */
		active_orders.removeAll();
		active_orders.setBackground(Color.WHITE);
		//active_orders.setLayout(new GridLayout(3,3,20,5));
		active_orders.setLayout(new MigLayout("wrap 2", "[grow,fill]"));
		
		String[] buttons = {"Finish Order", "Cancel Order", "Exit"};
		
		JLabel lblActiveOrders = new JLabel("Active Orders");
		lblActiveOrders.setFont(new Font("Dialog", Font.BOLD, 22));
		active_orders.add(lblActiveOrders, "wrap, center");
		
		String sql_curr_active = "SELECT order_id FROM ORDERS WHERE is_active = 1";
		ResultSet rs_curr_active = db.query(sql_curr_active);
		
		if (!rs_curr_active.isBeforeFirst() ) {    
			lblActiveOrders = new JLabel("No Active Orders");
			lblActiveOrders.setFont(new Font("Dialog", Font.BOLD, 30));
			lblActiveOrders.setForeground (Color.red);
			active_orders.add(lblActiveOrders, "wrap, center");
		} 
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayout(3,3,20,5));
		//active_orders.add(mainPanel, "pushx, pushy, growx, growy");

		active_orders.add(mainPanel, "pushx, pushy, growx, growy");
		
		
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		BevelBorder border;
		//TitledBorder tborder;
		JLabel label1;
		String t[]= new String[9];
		String t_qty[]= new String[9];

		String t_price[]= new String[9];
		
		
		int order_id[]= {0,0,0,0,0,0,0,0,0}; 
		
		String item_name[] = new String[200];				
		int quanti[]= new int[200]; 		
		double price[]= new double[200]; 
		double p_sum=0;
		
		int i=0,orderN=0,z=0;
		int ck=0; 
		int table_id[] = new int[9];
		int table_num[] = new int[9];
		String total[] = new String[9];
		
		int tableno[]={0,0,0,0,0,0,0,0,0};
		int table_index=1;
		
		
		
		JScrollPane scrollPane1 = new JScrollPane();
		JScrollPane scrollPane2 = new JScrollPane();
		JScrollPane scrollPane3 = new JScrollPane();
		JScrollPane scrollPane4 = new JScrollPane();
		JScrollPane scrollPane5 = new JScrollPane();
		JScrollPane scrollPane6 = new JScrollPane();
		JScrollPane scrollPane7 = new JScrollPane();
		JScrollPane scrollPane8 = new JScrollPane();
		JScrollPane scrollPane9 = new JScrollPane();
		
		JLabel lblTotal1 = new JLabel();
		JLabel lblOrderNo1 = new JLabel();
		JLabel lblTableNo1= new JLabel();
		JLabel lblTotal2 = new JLabel();
		JLabel lblOrderNo2 = new JLabel();
		JLabel lblTableNo2= new JLabel();
		JLabel lblTotal3 = new JLabel();
		JLabel lblOrderNo3 = new JLabel();
		JLabel lblTableNo3= new JLabel();
		JLabel lblTotal4 = new JLabel();
		JLabel lblOrderNo4 = new JLabel();
		JLabel lblTableNo4= new JLabel();
		JLabel lblTotal5 = new JLabel();
		JLabel lblOrderNo5 = new JLabel();
		JLabel lblTableNo5= new JLabel();
		JLabel lblTotal6 = new JLabel();
		JLabel lblOrderNo6 = new JLabel();
		JLabel lblTableNo6= new JLabel();
		JLabel lblTotal7 = new JLabel();
		JLabel lblOrderNo7 = new JLabel();
		JLabel lblTableNo7= new JLabel();
		JLabel lblTotal8 = new JLabel();
		JLabel lblOrderNo8 = new JLabel();
		JLabel lblTableNo8= new JLabel();
		JLabel lblTotal9 = new JLabel();
		JLabel lblOrderNo9 = new JLabel();
		JLabel lblTableNo9= new JLabel();
		
		
		JPanel panel1 = new JPanel(new MigLayout("wrap", "[grow,fill]"));
		JPanel panel2 = new JPanel(new MigLayout("wrap", "[grow,fill]"));
		JPanel panel3 = new JPanel(new MigLayout("wrap", "[grow,fill]"));
		JPanel panel4 = new JPanel(new MigLayout("wrap", "[grow,fill]"));
		JPanel panel5 = new JPanel(new MigLayout("wrap", "[grow,fill]"));
		JPanel panel6 = new JPanel(new MigLayout("wrap", "[grow,fill]"));
		JPanel panel7 = new JPanel(new MigLayout("wrap", "[grow,fill]"));
		JPanel panel8 = new JPanel(new MigLayout("wrap", "[grow,fill]"));
		JPanel panel9 = new JPanel(new MigLayout("wrap", "[grow,fill]"));
		MysqlConnect.getDbCon();
		
		/** trying to fix this ugly layout */
		
		
				
		//active_t.setLayout(new MigLayout("wrap","[grow]"));
		
		
		
		/** check the is_active */
		String check_sql = "SELECT order_id, table_id from ORDERS WHERE is_active=1";
		pstmt = db.conn.prepareStatement(check_sql);
		rs = pstmt.executeQuery();
		if (rs.next()) {
		    do {
		    	order_id[ck] = rs.getInt("order_id");
		    	table_id[ck] = rs.getInt("table_id");
				ck++;
		    } while(rs.next());
		} else {
			
		}
		
		String sql = "SELECT order_id ,item_name, quantity, price FROM TABLE_ORDERS WHERE "
						+"order_id ="+order_id[0]+ " OR"
						+" order_id =" +order_id[1]+ " OR"
						+" order_id =" +order_id[2]+ " OR"
						+" order_id =" +order_id[3]+ " OR"
						+" order_id =" +order_id[4]+ " OR"
						+" order_id =" +order_id[5]+ " OR"
						+" order_id =" +order_id[6]+ " OR"
						+" order_id =" +order_id[7]+ " OR"
						+" order_id =" +order_id[8];
		
		
		
																									; 
		pstmt = db.conn.prepareStatement(sql);
		rs = pstmt.executeQuery();
		
		
		if(rs.next()){
			
			orderN = rs.getInt("order_id");
			item_name[i] = rs.getString("item_name");
			quanti[i] = rs.getInt("quantity");
			price[i] = rs.getDouble("price");
			i++;
		}
		
		
		
		if (rs.next()) {
			do{
				if(orderN == rs.getInt("order_id")){
					item_name[i] = rs.getString("item_name");
					quanti[i] = rs.getInt("quantity");
					price[i] = rs.getDouble("price");
					i++;
				}else{
					switch(table_index){
						case 1:
							tableno[0] = i;
							table_index=2;
							break;
						case 2:
							tableno[1] = i;
							table_index=3;
							break;
						case 3:
							tableno[2] = i;
							table_index=4;
							break;
						case 4:
							tableno[3] = i;
							table_index=5;
							break;
						case 5:
							tableno[4] = i;
							table_index=6;
							break;
						case 6:
							tableno[5] = i;
							table_index=7;
							break;
						case 7:
							tableno[6] = i;
							table_index=8;
							break;
						case 8:
							tableno[7] = i;
							table_index=9;
							break;
						
						}				
						
						orderN = rs.getInt("order_id");
						item_name[i] = rs.getString("item_name");
						quanti[i] = rs.getInt("quantity");
						price[i] =rs.getDouble("price");	
						i++;
					}
			}while(rs.next());
		}
		/** make title */
		for(int k=0; k<9; k++){
			t[k]="";
			t_qty[k]="";
			t_price[k]="";
			// compare with initial data of order_id 
			// if order_id is same then it is empty
			if(order_id[0]==0 && k==0){
				t[k] = t[k]+ "EMPTY <BR>";
				tableno[k] =i;
			}
			else if(order_id[k]==0){
				t[k] = t[k]+ "EMPTY <BR>";
				tableno[k-1] =i;  // for last table index 
			}else{
				tableno[8] =i; // when 9 table is full "i" for last 9th table index
			}
		}
		
		/** table1*/
		p_sum=0;
		for(int j=0; j<tableno[0]; j++){
		t[0] = t[0]
				+ item_name[j]
				+ "<br>";
		
		t_qty[0] = t_qty[0]
				+ quanti[j]
				+ "<br>";
		
		t_price[0] = t_price[0]
				+ quanti[j]*price[j]
				+ "<br>";
		
		p_sum = p_sum + (price[j]*quanti[j]);
		}
		/** content for each table*/
		label1 = new JLabel(
				"<html><table><tr><td>"
				+"<div style='border: 0px '>"
				+t[0]
				+"</div></td><td width='40px'>"
				+ "</td><td>"
				+"<div style='border: 0px '>"
				+t_qty[0]
				+"</div></td><td width='70px'>"
				+ "</td><td>"
				+"<div style='border: 0px '>"
				+t_price[0]
				+"</div></td>"
				+"</tr></table></html>");
		total[0] =decimalFormat.format((p_sum*100)/100.0);
		lblTotal1.setText("Total: $" + total[0]);
		/** table2*/
		p_sum=0;
		for(int j=tableno[0]; j<tableno[1]; j++){
			t[1] = t[1]
					+ item_name[j]
					+ "<br>";
			
			t_qty[1] = t_qty[1]
					+ quanti[j]
					+ "<br>";
			
			t_price[1] = t_price[1]
					+ quanti[j]*price[j]
					+ "<br>";
			p_sum = p_sum + (price[j]*quanti[j]);			}
		JLabel label2 = new JLabel(
				"<html><table><tr><td>"
						+"<div style='border: 0px '>"
						+t[1]
						+"</div></td><td width='40px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_qty[1]
						+"</div></td><td width='70px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_price[1]
						+"</div></td>"
						+"</tr></table></html>");
		total[1] =decimalFormat.format((p_sum*100)/100.0);
		lblTotal2.setText("Total: $" +decimalFormat.format((p_sum*100)/100.0));
		/** table3*/
		p_sum=0;
		for(int j=tableno[1]; j<tableno[2]; j++){
			t[2] = t[2]
					+ item_name[j]
					+ "<br>";
			
			t_qty[2] = t_qty[2]
					+ quanti[j]
					+ "<br>";
			
			t_price[2] = t_price[2]
					+ quanti[j]*price[j]
					+ "<br>";
			p_sum = p_sum + (price[j]*quanti[j]);
			}
		JLabel label3 = new JLabel(
				"<html><table><tr><td>"
						+"<div style='border: 0px '>"
						+t[2]
						+"</div></td><td width='40px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_qty[2]
						+"</div></td><td width='70px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_price[2]
						+"</div></td>"
						+"</tr></table></html>");
		total[2] =decimalFormat.format((p_sum*100)/100.0);
		lblTotal3.setText("Total: $" +decimalFormat.format((p_sum*100)/100.0));
		/** table4*/
		p_sum=0;
		for(int j=tableno[2]; j<tableno[3]; j++){
			t[3] = t[3]
					+ item_name[j]
					+ "<br>";
			
			t_qty[3] = t_qty[3]
					+ quanti[j]
					+ "<br>";
			
			t_price[3] = t_price[3]
					+ quanti[j]*price[j]
					+ "<br>";
			p_sum = p_sum + (price[j]*quanti[j]);
			}
		JLabel label4 = new JLabel(
				"<html><table><tr><td>"
						+"<div style='border: 0px '>"
						+t[3]
						+"</div></td><td width='40px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_qty[3]
						+"</div></td><td width='70px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_price[3]
						+"</div></td>"
						+"</tr></table></html>");
		total[3] =decimalFormat.format((p_sum*100)/100.0);
		lblTotal4.setText("Total: $" +decimalFormat.format((p_sum*100)/100.0));
		/** table5*/
		p_sum=0;
		for(int j=tableno[3]; j<tableno[4]; j++){
			t[4] = t[4]
					+ item_name[j]
					+ "<br>";
			
			t_qty[4] = t_qty[4]
					+ quanti[j]
					+ "<br>";
			
			t_price[4] = t_price[4]
					+ quanti[j]*price[j]
					+ "<br>";
			p_sum = p_sum + (price[j]*quanti[j]);
			}
		JLabel label5 = new JLabel(
				"<html><table><tr><td>"
						+"<div style='border: 0px '>"
						+t[4]
						+"</div></td><td width='40px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_qty[4]
						+"</div></td><td width='70px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_price[4]
						+"</div></td>"
						+"</tr></table></html>");
		total[4] =decimalFormat.format((p_sum*100)/100.0);
		lblTotal5.setText("Total: $" +decimalFormat.format((p_sum*100)/100.0));
		/** table6*/
		p_sum=0;
		for(int j=tableno[4]; j<tableno[5]; j++){
			t[5] = t[5]
					+ item_name[j]
					+ "<br>";
			
			t_qty[5] = t_qty[5]
					+ quanti[j]
					+ "<br>";
			
			t_price[5] = t_price[5]
					+ quanti[j]*price[j]
					+ "<br>";
			p_sum = p_sum + (price[j]*quanti[j]);
			}
		JLabel label6 = new JLabel(
				"<html><table><tr><td>"
						+"<div style='border: 0px '>"
						+t[5]
						+"</div></td><td width='40px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_qty[5]
						+"</div></td><td width='70px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_price[5]
						+"</div></td>"
						+"</tr></table></html>");
		total[5] =decimalFormat.format((p_sum*100)/100.0);
		lblTotal6.setText("Total: $" +decimalFormat.format((p_sum*100)/100.0));
		/** table7*/
		p_sum=0;
		for(int j=tableno[5]; j<tableno[6]; j++){
			t[6] = t[6]
					+ item_name[j]
					+ "<br>";
			
			t_qty[6] = t_qty[6]
					+ quanti[j]
					+ "<br>";
			
			t_price[6] = t_price[6]
					+ quanti[j]*price[j]
					+ "<br>";
			p_sum = p_sum + (price[j]*quanti[j]);
			}
		JLabel label7 = new JLabel(
				"<html><table><tr><td>"
						+"<div style='border: 0px '>"
						+t[6]
						+"</div></td><td width='40px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_qty[6]
						+"</div></td><td width='70px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_price[6]
						+"</div></td>"
						+"</tr></table></html>");
		total[6] =decimalFormat.format((p_sum*100)/100.0);
		lblTotal7.setText("Total: $" +decimalFormat.format((p_sum*100)/100.0));
		/** table8*/
		p_sum=0;
		for(int j=tableno[6]; j<tableno[7]; j++){
			t[7] = t[7]
					+ item_name[j]
					+ "<br>";
			
			t_qty[7] = t_qty[7]
					+ quanti[j]
					+ "<br>";
			
			t_price[7] = t_price[7]
					+ quanti[j]*price[j]
					+ "<br>";
			p_sum = p_sum + (price[j]*quanti[j]);
			}
		JLabel label8 = new JLabel(
				"<html><table><tr><td>"
						+"<div style='border: 0px '>"
						+t[7]
						+"</div></td><td width='40px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_qty[7]
						+"</div></td><td width='70px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_price[7]
						+"</div></td>"
						+"</tr></table></html>");
		total[7] =decimalFormat.format((p_sum*100)/100.0);
		lblTotal8.setText("Total: $" +decimalFormat.format((p_sum*100)/100.0));
		/** table9*/
		p_sum=0;
		for(int j=tableno[7]; j<tableno[8]; j++){
			t[8] = t[8]
					+ item_name[j]
					+ "<br>";
			
			t_qty[8] = t_qty[8]
					+ quanti[j]
					+ "<br>";
			
			t_price[8] = t_price[8]
					+ quanti[j]*price[j]
					+ "<br>";
			p_sum = p_sum + (price[j]*quanti[j]);
			}
		JLabel label9 = new JLabel(
				"<html><table><tr><td>"
						+"<div style='border: 0px '>"
						+t[8]
						+"</div></td><td width='40px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_qty[8]
						+"</div></td><td width='70px'>"
						+ "</td><td>"
						+"<div style='border: 0px '>"
						+t_price[8]
						+"</div></td>"
						+"</tr></table></html>");
		total[8] =decimalFormat.format((p_sum*100)/100.0);
		lblTotal9.setText("Total: $" +decimalFormat.format((p_sum*100)/100.0));
		
		
		/**title label border*/		
		border = new BevelBorder(BevelBorder.RAISED);
		//tborder=new TitledBorder("");
		//tborder.setTitlePosition(TitledBorder.ABOVE_TOP);
		//tborder.setTitleJustification(TitledBorder.CENTER);
		
		/** labels for table info*/
		label1.setFont(new Font("Dialog", Font.BOLD, 13));
		label1.setOpaque(true);
		label1.setBackground(Color.WHITE);
		label1.setBorder(border);
		label2.setFont(new Font("Dialog", Font.BOLD, 13));
		label2.setBorder(border);
		label2.setOpaque(true);
		label2.setBackground(Color.WHITE);
		label3.setFont(new Font("Dialog", Font.BOLD, 13));
		label3.setBorder(border);
		label3.setOpaque(true);
		label3.setBackground(Color.WHITE);
		label4.setFont(new Font("Dialog", Font.BOLD, 13));
		label4.setBorder(border);
		label4.setOpaque(true);
		label4.setBackground(Color.WHITE);
		label5.setFont(new Font("Dialog", Font.BOLD, 13));
		label5.setBorder(border);
		label5.setOpaque(true);
		label5.setBackground(Color.WHITE);
		label6.setFont(new Font("Dialog", Font.BOLD, 13));
		label6.setBorder(border);
		label6.setOpaque(true);
		label6.setBackground(Color.WHITE);
		label7.setFont(new Font("Dialog", Font.BOLD, 13));
		label7.setBorder(border);
		label7.setOpaque(true);
		label7.setBackground(Color.WHITE);
		label8.setFont(new Font("Dialog", Font.BOLD, 13));
		label8.setBorder(border);
		label8.setOpaque(true);
		label8.setBackground(Color.WHITE);
		label9.setFont(new Font("Dialog", Font.BOLD, 13));
		label9.setBorder(border);
		label9.setOpaque(true);
		label9.setBackground(Color.WHITE);
		String sql_tbl = "SELECT table_no "
				+ "FROM ORDERS O "
				+ "LEFT JOIN TABLES T "
				+ "ON O.table_id = T.table_id "
				+ "WHERE O.order_id ="+order_id[0]+ " OR"
				+" O.order_id =" +order_id[1]+ " OR"
				+" O.order_id =" +order_id[2]+ " OR"
				+" O.order_id =" +order_id[3]+ " OR"
				+" O.order_id =" +order_id[4]+ " OR"
				+" O.order_id =" +order_id[5]+ " OR"
				+" O.order_id =" +order_id[6]+ " OR"
				+" O.order_id =" +order_id[7]+ " OR"
				+" O.order_id =" +order_id[8];
		
		pstmt = db.conn.prepareStatement(sql_tbl);
		rs = pstmt.executeQuery();
		while(rs.next()){
			table_num[z] = rs.getInt("table_no");
			z++;
		}
		lblOrderNo1.setText("Order No: "+table_id[0]);
		lblTableNo1.setText("Table No: "+table_num[0]);
		
		lblOrderNo2.setText("Order No: "+table_id[1]);
		lblTableNo2.setText("Table No: "+table_num[1]);
		lblOrderNo3.setText("Order No: "+table_id[2]);
		lblTableNo3.setText("Table No: "+table_num[2]);
		lblOrderNo4.setText("Order No: "+table_id[3]);
		lblTableNo4.setText("Table No: "+table_num[3]);
		lblOrderNo5.setText("Order No: "+table_id[4]);
		lblTableNo5.setText("Table No: "+table_num[4]);
		lblOrderNo6.setText("Order No: "+table_id[5]);
		lblTableNo6.setText("Table No: "+table_num[5]);
		lblOrderNo7.setText("Order No: "+table_id[6]);
		lblTableNo7.setText("Table No: "+table_num[6]);
		lblOrderNo8.setText("Order No: "+table_id[7]);
		lblTableNo8.setText("Table No: "+table_num[7]);
		lblOrderNo9.setText("Order No: "+table_id[8]);
		lblTableNo9.setText("Table No: "+table_num[8]);
		
		lblTableNo1.setFont(new Font("Dialog", Font.BOLD, 17));
		lblTableNo2.setFont(new Font("Dialog", Font.BOLD, 17));
		lblTableNo3.setFont(new Font("Dialog", Font.BOLD, 17));
		lblTableNo4.setFont(new Font("Dialog", Font.BOLD, 17));
		lblTableNo5.setFont(new Font("Dialog", Font.BOLD, 17));
		lblTableNo6.setFont(new Font("Dialog", Font.BOLD, 17));
		lblTableNo7.setFont(new Font("Dialog", Font.BOLD, 17));
		lblTableNo8.setFont(new Font("Dialog", Font.BOLD, 17));
		lblTableNo9.setFont(new Font("Dialog", Font.BOLD, 17));
		//lblTableNo.setText("Table No: "+table_no);
		/** should to fix */
	
		scrollPane1.setPreferredSize(new Dimension(200,200));
		mainPanel.add(panel1);
		panel1.add(lblTableNo1);
		panel1.add(scrollPane1);
		panel1.add(lblOrderNo1);
		panel1.add(lblTotal1);
		scrollPane1.setViewportView(label1);
		scrollPane1.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		scrollPane2.setPreferredSize(new Dimension(200,200));
		mainPanel.add(panel2);
		panel2.add(lblTableNo2);
		panel2.add(scrollPane2);
		panel2.add(lblOrderNo2);
		panel2.add(lblTotal2);
		scrollPane2.setViewportView(label2);
		scrollPane2.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		scrollPane3.setPreferredSize(new Dimension(200,200));
		mainPanel.add(panel3);
		panel3.add(lblTableNo3);
		panel3.add(scrollPane3);
		panel3.add(lblOrderNo3);
		panel3.add(lblTotal3);
		scrollPane3.setViewportView(label3);
		scrollPane3.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane3.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		scrollPane4.setPreferredSize(new Dimension(200,200));
		mainPanel.add(panel4);
		panel4.add(lblTableNo4);
		panel4.add(scrollPane4);
		panel4.add(lblOrderNo4);
		panel4.add(lblTotal4);
		scrollPane4.setViewportView(label4);
		scrollPane4.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane4.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		scrollPane5.setPreferredSize(new Dimension(200,200));
		mainPanel.add(panel5);
		panel5.add(lblTableNo5);
		panel5.add(scrollPane5);
		panel5.add(lblOrderNo5);
		panel5.add(lblTotal5);
		scrollPane5.setViewportView(label5);
		scrollPane5.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane5.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		scrollPane6.setPreferredSize(new Dimension(200,200));
		mainPanel.add(panel6);
		panel6.add(lblTableNo6);
		panel6.add(scrollPane6);
		panel6.add(lblOrderNo6);
		panel6.add(lblTotal6);
		scrollPane6.setViewportView(label6);
		scrollPane6.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane6.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		scrollPane7.setPreferredSize(new Dimension(200,200));
		mainPanel.add(panel7);
		panel7.add(lblTableNo7);
		panel7.add(scrollPane7);
		panel7.add(lblOrderNo7);
		panel7.add(lblTotal7);
		scrollPane7.setViewportView(label7);
		scrollPane7.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane7.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		scrollPane8.setPreferredSize(new Dimension(200,200));
		mainPanel.add(panel8);
		panel8.add(lblTableNo8);
		panel8.add(scrollPane8);
		panel8.add(lblOrderNo8);
		panel8.add(lblTotal8);
		scrollPane8.setViewportView(label8);
		scrollPane8.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane8.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		scrollPane9.setPreferredSize(new Dimension(200,200));
		mainPanel.add(panel9);
		panel9.add(lblTableNo9);
		panel9.add(scrollPane9);
		panel9.add(lblOrderNo9);
		panel9.add(lblTotal9);
		scrollPane9.setViewportView(label9);
		scrollPane9.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane9.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		
		/** click listener */
		label1.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				
				if(order_id[0] !=0){
					int result = JOptionPane.showOptionDialog(null, "Select option to process:",
							"Active order ("+order_id[0]+")", JOptionPane.YES_NO_CANCEL_OPTION, 
							JOptionPane.QUESTION_MESSAGE, null, buttons, "second_value");
					switch(result){
						case 0: 
							//when choose completed code
							JCheckBox payCash = new JCheckBox("Pay Cash");
							JCheckBox payCredit = new JCheckBox("Pay Credit");
							String msg = "Select payment type:"; 
							
							Object[] msgContent = {msg, payCash, payCredit}; 
							int reply_f = JOptionPane.showConfirmDialog ( null,  msgContent,  "Payment Type", JOptionPane.YES_NO_OPTION); 
							boolean cash = payCash.isSelected();
							boolean credit = payCredit.isSelected();
							
						if (reply_f== JOptionPane.YES_OPTION) {
							if(cash == true && credit == true){
								
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
												   + "SET p_type_id   = NULL, "
													   + "p_type_name = 'CASH', "
													   + "bill_id     = "+order_id[0]+" ";
								
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[0]+" ";
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(cash == true && credit != true){
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
										           + "SET p_type_id   = NULL, "
										               + "p_type_name = 'CASH', "
										               + "bill_id     = "+order_id[0]+" ";
								
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(credit == true && cash != true ){
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[0]+" ";
								try {
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
							}
							String sql_u = "UPDATE ORDERS O "
								      + "LEFT JOIN BILL B "
							             + "ON B.bill_id = O.bill_id "
							            + "SET B.total = "+total[0]+", "
							                + "O.is_active = 0 "
							          + "WHERE O.order_id = "+ order_id[0]+" ";
											
							 try {
								 	PreparedStatement pstmt_u = db.conn.prepareStatement(sql_u);
									pstmt_u.executeUpdate();
									activeOrders();
								
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
							 }else {
						        	JOptionPane.getRootFrame().dispose();  
						     }
								
							break;
						case 1:
							//when choose cancel code
							String sql_c =  "UPDATE ORDERS O "
									   + "LEFT JOIN BILL B "
								   		+ "ON B.bill_id = O.bill_id "
							             + "SET B.total = "+total[0]+", "
							             + "O.is_active = 0, "
							             + "O.is_canceled = 1 "
							             + "WHERE O.order_id = " + order_id[0];
					
							String message = "Are you sure you want to cancel this order?";
							String title = "Cancel Order ("+order_id[0]+")";
							 
							int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
						        if (reply == JOptionPane.YES_OPTION) {
						        	try {
						        		PreparedStatement pstmt_u = db.conn.prepareStatement(sql_c);
										pstmt_u.executeUpdate();
										activeOrders();
									} catch (SQLException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
						        } else {
						        	JOptionPane.getRootFrame().dispose();  
						        }
								
						
							break;
						
					}
				}
			}
		});
		label2.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				
				if(order_id[1] !=0){
					int result = JOptionPane.showOptionDialog(null, "Select option to process:",
							"Active order ("+order_id[1]+")", JOptionPane.YES_NO_CANCEL_OPTION, 
							JOptionPane.QUESTION_MESSAGE, null, buttons, "second_value");
					switch(result){
						case 0: 
							//when choose completed code
							JCheckBox payCash = new JCheckBox("Pay Cash");
							JCheckBox payCredit = new JCheckBox("Pay Credit");
							String msg = "Select payment type:"; 
							
							Object[] msgContent = {msg, payCash, payCredit}; 
							int reply_f = JOptionPane.showConfirmDialog ( null,  msgContent,  "Payment Type", JOptionPane.YES_NO_OPTION); 
							boolean cash = payCash.isSelected();
							boolean credit = payCredit.isSelected();
							
						if (reply_f== JOptionPane.YES_OPTION) {
							if(cash == true && credit == true){
								
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
												   + "SET p_type_id   = NULL, "
													   + "p_type_name = 'CASH', "
													   + "bill_id     = "+order_id[1]+" ";
								
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[1]+" ";
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(cash == true && credit != true){
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
										           + "SET p_type_id   = NULL, "
										               + "p_type_name = 'CASH', "
										               + "bill_id     = "+order_id[1]+" ";
								
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(credit == true && cash != true ){
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[1]+" ";
								try {
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
							}
							String sql_u = "UPDATE ORDERS O "
								      + "LEFT JOIN BILL B "
							             + "ON B.bill_id = O.bill_id "
							            + "SET B.total = "+total[1]+", "
							                + "O.is_active = 0 "
							          + "WHERE O.order_id = "+ order_id[1]+" ";
											
							 try {
								 	PreparedStatement pstmt_u = db.conn.prepareStatement(sql_u);
									pstmt_u.executeUpdate();
									activeOrders();
								
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
							 }else {
						        	JOptionPane.getRootFrame().dispose();  
						     }
								
							break;
						case 1:
							//when choose cancel code
							String sql_c =  "UPDATE ORDERS O "
									   + "LEFT JOIN BILL B "
								   		+ "ON B.bill_id = O.bill_id "
							             + "SET B.total = "+total[1]+", "
							             + "O.is_active = 0, "
							             + "O.is_canceled = 1 "
							             + "WHERE O.order_id = " + order_id[1];
					
							String message = "Are you sure you want to cancel this order?";
							String title = "Cancel Order ("+order_id[1]+")";
							 
							int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
						        if (reply == JOptionPane.YES_OPTION) {
						        	try {
						        		PreparedStatement pstmt_u = db.conn.prepareStatement(sql_c);
										pstmt_u.executeUpdate();
										activeOrders();
									} catch (SQLException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
						        } else {
						        	JOptionPane.getRootFrame().dispose();  
						        }
								
						
							break;
						
					}
				}
			}
		});
		
		label3.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				
				if(order_id[2] !=0){
					int result = JOptionPane.showOptionDialog(null, "Select option to process:",
							"Active order ("+order_id[2]+")", JOptionPane.YES_NO_CANCEL_OPTION, 
							JOptionPane.QUESTION_MESSAGE, null, buttons, "second_value");
					switch(result){
						case 0: 
							//when choose completed code
							JCheckBox payCash = new JCheckBox("Pay Cash");
							JCheckBox payCredit = new JCheckBox("Pay Credit");
							String msg = "Select payment type:"; 
							
							Object[] msgContent = {msg, payCash, payCredit}; 
							int reply_f = JOptionPane.showConfirmDialog ( null,  msgContent,  "Payment Type", JOptionPane.YES_NO_OPTION); 
							boolean cash = payCash.isSelected();
							boolean credit = payCredit.isSelected();
							
						if (reply_f== JOptionPane.YES_OPTION) {
							if(cash == true && credit == true){
								
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
												   + "SET p_type_id   = NULL, "
													   + "p_type_name = 'CASH', "
													   + "bill_id     = "+order_id[2]+" ";
								
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[2]+" ";
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(cash == true && credit != true){
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
										           + "SET p_type_id   = NULL, "
										               + "p_type_name = 'CASH', "
										               + "bill_id     = "+order_id[2]+" ";
								
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(credit == true && cash != true ){
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[2]+" ";
								try {
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
							}
							String sql_u = "UPDATE ORDERS O "
								      + "LEFT JOIN BILL B "
							             + "ON B.bill_id = O.bill_id "
							            + "SET B.total = "+total[2]+", "
							                + "O.is_active = 0 "
							          + "WHERE O.order_id = "+ order_id[2]+" ";
											
							 try {
								 	PreparedStatement pstmt_u = db.conn.prepareStatement(sql_u);
									pstmt_u.executeUpdate();
									activeOrders();
								
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
							 }else {
						        	JOptionPane.getRootFrame().dispose();  
						     }
								
							
							break;
						case 1:
							//when choose cancel code
							String sql_c =  "UPDATE ORDERS O "
									   + "LEFT JOIN BILL B "
								   		+ "ON B.bill_id = O.bill_id "
							             + "SET B.total = "+total[2]+", "
							             + "O.is_active = 0, "
							             + "O.is_canceled = 1 "
							             + "WHERE O.order_id = " + order_id[2];
					
							String message = "Are you sure you want to cancel this order?";
							String title = "Cancel Order ("+order_id[2]+")";
							 
							int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
						        if (reply == JOptionPane.YES_OPTION) {
						        	try {
						        		PreparedStatement pstmt_u = db.conn.prepareStatement(sql_c);
										pstmt_u.executeUpdate();
										activeOrders();
									} catch (SQLException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
						        } else {
						        	JOptionPane.getRootFrame().dispose();  
						        }
								
						
							break;
						
					}
				}
			}
		});
		
		label4.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				
				if(order_id[3] !=0){
					int result = JOptionPane.showOptionDialog(null, "Select option to process:",
							"Active order ("+order_id[3]+")", JOptionPane.YES_NO_CANCEL_OPTION, 
							JOptionPane.QUESTION_MESSAGE, null, buttons, "second_value");
					switch(result){
						case 0: 
							//when choose completed code
							JCheckBox payCash = new JCheckBox("Pay Cash");
							JCheckBox payCredit = new JCheckBox("Pay Credit");
							String msg = "Select payment type:"; 
							
							Object[] msgContent = {msg, payCash, payCredit}; 
							int reply_f = JOptionPane.showConfirmDialog ( null,  msgContent,  "Payment Type", JOptionPane.YES_NO_OPTION); 
							boolean cash = payCash.isSelected();
							boolean credit = payCredit.isSelected();
							
						if (reply_f== JOptionPane.YES_OPTION) {
							if(cash == true && credit == true){
								
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
												   + "SET p_type_id   = NULL, "
													   + "p_type_name = 'CASH', "
													   + "bill_id     = "+order_id[3]+" ";
								
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[3]+" ";
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(cash == true && credit != true){
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
										           + "SET p_type_id   = NULL, "
										               + "p_type_name = 'CASH', "
										               + "bill_id     = "+order_id[3]+" ";
								
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(credit == true && cash != true ){
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[3]+" ";
								try {
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
							}
							String sql_u = "UPDATE ORDERS O "
								      + "LEFT JOIN BILL B "
							             + "ON B.bill_id = O.bill_id "
							            + "SET B.total = "+total[3]+", "
							                + "O.is_active = 0 "
							          + "WHERE O.order_id = "+ order_id[3]+" ";
											
							 try {
								 	PreparedStatement pstmt_u = db.conn.prepareStatement(sql_u);
									pstmt_u.executeUpdate();
									activeOrders();
								
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
							 }else {
						        	JOptionPane.getRootFrame().dispose();  
						     }
								
							
							break;
						case 1:
							//when choose cancel code
							String sql_c =  "UPDATE ORDERS O "
									   + "LEFT JOIN BILL B "
								   		+ "ON B.bill_id = O.bill_id "
							             + "SET B.total = "+total[3]+", "
							             + "O.is_active = 0, "
							             + "O.is_canceled = 1 "
							             + "WHERE O.order_id = " + order_id[3];
					
							String message = "Are you sure you want to cancel this order?";
							String title = "Cancel Order ("+order_id[3]+")";
							 
							int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
						        if (reply == JOptionPane.YES_OPTION) {
						        	try {
						        		PreparedStatement pstmt_u = db.conn.prepareStatement(sql_c);
										pstmt_u.executeUpdate();
										activeOrders();
									} catch (SQLException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
						        } else {
						        	JOptionPane.getRootFrame().dispose();  
						        }
								
						
							break;
						
					}
				}
			}
		});
		
		label5.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				
				if(order_id[4] !=0){
					int result = JOptionPane.showOptionDialog(null, "Select option to process:",
							"Active order ("+order_id[4]+")", JOptionPane.YES_NO_CANCEL_OPTION, 
							JOptionPane.QUESTION_MESSAGE, null, buttons, "second_value");
					switch(result){
						case 0: 
							//when choose completed code
							JCheckBox payCash = new JCheckBox("Pay Cash");
							JCheckBox payCredit = new JCheckBox("Pay Credit");
							String msg = "Select payment type:"; 
							
							Object[] msgContent = {msg, payCash, payCredit}; 
							int reply_f = JOptionPane.showConfirmDialog ( null,  msgContent,  "Payment Type", JOptionPane.YES_NO_OPTION); 
							boolean cash = payCash.isSelected();
							boolean credit = payCredit.isSelected();
							
						if (reply_f== JOptionPane.YES_OPTION) {
							if(cash == true && credit == true){
								
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
												   + "SET p_type_id   = NULL, "
													   + "p_type_name = 'CASH', "
													   + "bill_id     = "+order_id[4]+" ";
								
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[4]+" ";
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(cash == true && credit != true){
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
										           + "SET p_type_id   = NULL, "
										               + "p_type_name = 'CASH', "
										               + "bill_id     = "+order_id[4]+" ";
								
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(credit == true && cash != true ){
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[4]+" ";
								try {
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
							}
							String sql_u = "UPDATE ORDERS O "
								      + "LEFT JOIN BILL B "
							             + "ON B.bill_id = O.bill_id "
							            + "SET B.total = "+total[4]+", "
							                + "O.is_active = 0 "
							          + "WHERE O.order_id = "+ order_id[4]+" ";
											
							 try {
								 	PreparedStatement pstmt_u = db.conn.prepareStatement(sql_u);
									pstmt_u.executeUpdate();
									activeOrders();
								
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
							 }else {
						        	JOptionPane.getRootFrame().dispose();  
						     }
								
							break;
						case 1:
							//when choose cancel code
							String sql_c =  "UPDATE ORDERS O "
									   + "LEFT JOIN BILL B "
								   		+ "ON B.bill_id = O.bill_id "
							             + "SET B.total = "+total[4]+", "
							             + "O.is_active = 0, "
							             + "O.is_canceled = 1 "
							             + "WHERE O.order_id = " + order_id[4];
					
							String message = "Are you sure you want to cancel this order?";
							String title = "Cancel Order ("+order_id[4]+")";
							 
							int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
						        if (reply == JOptionPane.YES_OPTION) {
						        	try {
						        		PreparedStatement pstmt_u = db.conn.prepareStatement(sql_c);
										pstmt_u.executeUpdate();
										activeOrders();
									} catch (SQLException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
						        } else {
						        	JOptionPane.getRootFrame().dispose();  
						        }
								
						
							break;
						
					}
				}
			}
		});
		
		label6.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				
				if(order_id[5] !=0){
					int result = JOptionPane.showOptionDialog(null, "Select option to process:",
							"Active order ("+order_id[5]+")", JOptionPane.YES_NO_CANCEL_OPTION, 
							JOptionPane.QUESTION_MESSAGE, null, buttons, "second_value");
					switch(result){
						case 0: 
							//when choose completed code
							JCheckBox payCash = new JCheckBox("Pay Cash");
							JCheckBox payCredit = new JCheckBox("Pay Credit");
							String msg = "Select payment type:"; 
							
							Object[] msgContent = {msg, payCash, payCredit}; 
							int reply_f = JOptionPane.showConfirmDialog ( null,  msgContent,  "Payment Type", JOptionPane.YES_NO_OPTION); 
							boolean cash = payCash.isSelected();
							boolean credit = payCredit.isSelected();
							
						if (reply_f== JOptionPane.YES_OPTION) {
							if(cash == true && credit == true){
								
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
												   + "SET p_type_id   = NULL, "
													   + "p_type_name = 'CASH', "
													   + "bill_id     = "+order_id[5]+" ";
								
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[5]+" ";
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(cash == true && credit != true){
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
										           + "SET p_type_id   = NULL, "
										               + "p_type_name = 'CASH', "
										               + "bill_id     = "+order_id[5]+" ";
								
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(credit == true && cash != true ){
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[5]+" ";
								try {
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
							}
							String sql_u = "UPDATE ORDERS O "
								      + "LEFT JOIN BILL B "
							             + "ON B.bill_id = O.bill_id "
							            + "SET B.total = "+total[5]+", "
							                + "O.is_active = 0 "
							          + "WHERE O.order_id = "+ order_id[5]+" ";
											
							 try {
								 	PreparedStatement pstmt_u = db.conn.prepareStatement(sql_u);
									pstmt_u.executeUpdate();
									activeOrders();
								
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
							 }else {
						        	JOptionPane.getRootFrame().dispose();  
						     }
							break;
						case 1:
							//when choose cancel code
							String sql_c =  "UPDATE ORDERS O "
									   + "LEFT JOIN BILL B "
								   		+ "ON B.bill_id = O.bill_id "
							             + "SET B.total = "+total[5]+", "
							             + "O.is_active = 0, "
							             + "O.is_canceled = 1 "
							             + "WHERE O.order_id = " + order_id[5];
					
							String message = "Are you sure you want to cancel this order?";
							String title = "Cancel Order ("+order_id[5]+")";
							 
							int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
						        if (reply == JOptionPane.YES_OPTION) {
						        	try {
						        		PreparedStatement pstmt_u = db.conn.prepareStatement(sql_c);
										pstmt_u.executeUpdate();
										activeOrders();
									} catch (SQLException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
						        } else {
						        	JOptionPane.getRootFrame().dispose();  
						        }
								
						
							break;
						
					}
				}
			}
		});
		
		label7.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				
				if(order_id[6] !=0){
					int result = JOptionPane.showOptionDialog(null, "Select option to process:",
							"Active order ("+order_id[6]+")", JOptionPane.YES_NO_CANCEL_OPTION, 
							JOptionPane.QUESTION_MESSAGE, null, buttons, "second_value");
					switch(result){
						case 0: 
							//when choose completed code
							JCheckBox payCash = new JCheckBox("Pay Cash");
							JCheckBox payCredit = new JCheckBox("Pay Credit");
							String msg = "Select payment type:"; 
							
							Object[] msgContent = {msg, payCash, payCredit}; 
							int reply_f = JOptionPane.showConfirmDialog ( null,  msgContent,  "Payment Type", JOptionPane.YES_NO_OPTION); 
							boolean cash = payCash.isSelected();
							boolean credit = payCredit.isSelected();
							
						if (reply_f== JOptionPane.YES_OPTION) {
							if(cash == true && credit == true){
								
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
												   + "SET p_type_id   = NULL, "
													   + "p_type_name = 'CASH', "
													   + "bill_id     = "+order_id[6]+" ";
								
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[6]+" ";
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(cash == true && credit != true){
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
										           + "SET p_type_id   = NULL, "
										               + "p_type_name = 'CASH', "
										               + "bill_id     = "+order_id[6]+" ";
								
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(credit == true && cash != true ){
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[6]+" ";
								try {
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
							}
							String sql_u = "UPDATE ORDERS O "
								      + "LEFT JOIN BILL B "
							             + "ON B.bill_id = O.bill_id "
							            + "SET B.total = "+total[6]+", "
							                + "O.is_active = 0 "
							          + "WHERE O.order_id = "+ order_id[6]+" ";
											
							 try {
								 	PreparedStatement pstmt_u = db.conn.prepareStatement(sql_u);
									pstmt_u.executeUpdate();
									activeOrders();
								
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
							 }else {
						        	JOptionPane.getRootFrame().dispose();  
						     }
								
							
							break;
						case 1:
							//when choose cancel code
							String sql_c =  "UPDATE ORDERS O "
									   + "LEFT JOIN BILL B "
								   		+ "ON B.bill_id = O.bill_id "
							             + "SET B.total = "+total[6]+", "
							             + "O.is_active = 0, "
							             + "O.is_canceled = 1 "
							             + "WHERE O.order_id = " + order_id[6];
					
							String message = "Are you sure you want to cancel this order?";
							String title = "Cancel Order ("+order_id[6]+")";
							 
							int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
						        if (reply == JOptionPane.YES_OPTION) {
						        	try {
						        		PreparedStatement pstmt_u = db.conn.prepareStatement(sql_c);
										pstmt_u.executeUpdate();
										activeOrders();
									} catch (SQLException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
						        } else {
						        	JOptionPane.getRootFrame().dispose();  
						        }
								
						
							break;
						
					}
				}
			}
		});
		
		label8.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				
				if(order_id[7] !=0){
					int result = JOptionPane.showOptionDialog(null, "Select option to process:",
							"Active order ("+order_id[7]+")", JOptionPane.YES_NO_CANCEL_OPTION, 
							JOptionPane.QUESTION_MESSAGE, null, buttons, "second_value");
					switch(result){
						case 0: 
							//when choose completed code
							JCheckBox payCash = new JCheckBox("Pay Cash");
							JCheckBox payCredit = new JCheckBox("Pay Credit");
							String msg = "Select payment type:"; 
							
							Object[] msgContent = {msg, payCash, payCredit}; 
							int reply_f = JOptionPane.showConfirmDialog ( null,  msgContent,  "Payment Type", JOptionPane.YES_NO_OPTION); 
							boolean cash = payCash.isSelected();
							boolean credit = payCredit.isSelected();
							
						if (reply_f== JOptionPane.YES_OPTION) {
							if(cash == true && credit == true){
								
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
												   + "SET p_type_id   = NULL, "
													   + "p_type_name = 'CASH', "
													   + "bill_id     = "+order_id[7]+" ";
								
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[7]+" ";
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(cash == true && credit != true){
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
										           + "SET p_type_id   = NULL, "
										               + "p_type_name = 'CASH', "
										               + "bill_id     = "+order_id[7]+" ";
								
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(credit == true && cash != true ){
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[7]+" ";
								try {
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
							}
							String sql_u = "UPDATE ORDERS O "
								      + "LEFT JOIN BILL B "
							             + "ON B.bill_id = O.bill_id "
							            + "SET B.total = "+total[7]+", "
							                + "O.is_active = 0 "
							          + "WHERE O.order_id = "+ order_id[7]+" ";
											
							 try {
								 	PreparedStatement pstmt_u = db.conn.prepareStatement(sql_u);
									pstmt_u.executeUpdate();
									activeOrders();
								
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
							 }else {
						        	JOptionPane.getRootFrame().dispose();  
						     }
								
							
							break;
						case 1:
							//when choose cancel code
							String sql_c =  "UPDATE ORDERS O "
									   + "LEFT JOIN BILL B "
								   		+ "ON B.bill_id = O.bill_id "
							             + "SET B.total = "+total[7]+", "
							             + "O.is_active = 0, "
							             + "O.is_canceled = 1 "
							             + "WHERE O.order_id = " + order_id[7];
					
							String message = "Are you sure you want to cancel this order?";
							String title = "Cancel Order ("+order_id[7]+")";
							 
							int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
						        if (reply == JOptionPane.YES_OPTION) {
						        	try {
						        		PreparedStatement pstmt_u = db.conn.prepareStatement(sql_c);
										pstmt_u.executeUpdate();
										activeOrders();
									} catch (SQLException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
						        } else {
						        	JOptionPane.getRootFrame().dispose();  
						        }
								
						
							break;
						
					}
				}
			}
		});
		
		label9.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				
				if(order_id[8] !=0){
					int result = JOptionPane.showOptionDialog(null, "Select option to process:",
							"Active order ("+order_id[0]+")", JOptionPane.YES_NO_CANCEL_OPTION, 
							JOptionPane.QUESTION_MESSAGE, null, buttons, "second_value");
					switch(result){
						case 0: 
							//when choose completed code
							JCheckBox payCash = new JCheckBox("Pay Cash");
							JCheckBox payCredit = new JCheckBox("Pay Credit");
							String msg = "Select payment type:"; 
							
							Object[] msgContent = {msg, payCash, payCredit}; 
							int reply_f = JOptionPane.showConfirmDialog ( null,  msgContent,  "Payment Type", JOptionPane.YES_NO_OPTION); 
							boolean cash = payCash.isSelected();
							boolean credit = payCredit.isSelected();
							
						if (reply_f== JOptionPane.YES_OPTION) {
							if(cash == true && credit == true){
								
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
												   + "SET p_type_id   = NULL, "
													   + "p_type_name = 'CASH', "
													   + "bill_id     = "+order_id[8]+" ";
								
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[8]+" ";
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(cash == true && credit != true){
								String sql_payCash = "INSERT INTO PAYMENT_TYPE "
										           + "SET p_type_id   = NULL, "
										               + "p_type_name = 'CASH', "
										               + "bill_id     = "+order_id[8]+" ";
								
								try {
									PreparedStatement pstmt_cs = db.conn.prepareStatement(sql_payCash);
									pstmt_cs.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
								
							}else if(credit == true && cash != true ){
								String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
										             + "SET p_type_id   = NULL, "
										                 + "p_type_name = 'CARD', "
										                 + "bill_id     = "+order_id[8]+" ";
								try {
									PreparedStatement pstmt_crd = db.conn.prepareStatement(sql_payCredit);
									pstmt_crd.executeUpdate();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}	
							}
							String sql_u = "UPDATE ORDERS O "
								      + "LEFT JOIN BILL B "
							             + "ON B.bill_id = O.bill_id "
							            + "SET B.total = "+total[8]+", "
							                + "O.is_active = 0 "
							          + "WHERE O.order_id = "+ order_id[8]+" ";
											
							 try {
								 	PreparedStatement pstmt_u = db.conn.prepareStatement(sql_u);
									pstmt_u.executeUpdate();
									activeOrders();
								
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
							 }else {
						        	JOptionPane.getRootFrame().dispose();  
						     }
								
							
							break;
						case 1:
							//when choose cancel code
							String sql_c =  "UPDATE ORDERS O "
									   + "LEFT JOIN BILL B "
								   		+ "ON B.bill_id = O.bill_id "
							             + "SET B.total = "+total[8]+", "
							             + "O.is_active = 0, "
							             + "O.is_canceled = 1 "
							             + "WHERE O.order_id = " + order_id[8];
					
							String message = "Are you sure you want to cancel this order?";
							String title = "Cancel Order ("+order_id[8]+")";
							 
							int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
						        if (reply == JOptionPane.YES_OPTION) {
						        	try {
						        		PreparedStatement pstmt_u = db.conn.prepareStatement(sql_c);
										pstmt_u.executeUpdate();
										activeOrders();
									} catch (SQLException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
						        } else {
						        	JOptionPane.getRootFrame().dispose();  
						        }
								
						
							break;
						
					}
				}
			}
		});
		
		
		
	
		return active_orders;
	}
}
