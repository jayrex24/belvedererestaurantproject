package belvedererestaurantproject;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import com.sun.glass.events.KeyEvent;
import net.miginfocom.swing.MigLayout;

/**
 @note implementing all interface with panels to be added into JFrame
 */
public class Main implements AddOrders, ActiveOrders, CompletedOrders, CanceledOrders, Statistics, Employees{
	
	private JFrame frmBelvedereTowersCafe;

	/** Launch the application. */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmBelvedereTowersCafe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws SQLException 
	 * @throws IOException 
	 */
	public Main() throws SQLException, IOException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 * @throws IOException 
	 */
	public void initialize() throws SQLException, IOException{	
		
		JMenuBar menuBar = new JMenuBar();
		
		/** Create Frame */
		frmBelvedereTowersCafe = new JFrame();
		frmBelvedereTowersCafe.setTitle("Belvedere Towers Cafe");
		frmBelvedereTowersCafe.setSize(1300, 780);
		frmBelvedereTowersCafe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBelvedereTowersCafe.setJMenuBar(menuBar);
		
		URL getImage = Main.class.getResource("/resources/btc_solutions_icon.png");
		ImageIcon img = new ImageIcon(getImage);
		frmBelvedereTowersCafe.setIconImage(img.getImage());
		
	    JMenu fileMenu = new JMenu("Admin");
	    fileMenu.setMnemonic(KeyEvent.VK_F);
	    menuBar.add(fileMenu);

	    JMenuItem loginMenuItem = new JMenuItem("Log In");
	    fileMenu.add(loginMenuItem);
	    
	    JMenuItem logoutMenuItem = new JMenuItem("Log Out");
		
		JPanel activeOrdersRefreshed = new JPanel();
		activeOrdersRefreshed.setBackground(Color.WHITE);	
		activeOrdersRefreshed.setLayout(new MigLayout("wrap", "[grow,fill]"));
		
		JPanel completedOrderRefreshed = new JPanel();
		completedOrderRefreshed.setBackground(Color.WHITE);	
		completedOrderRefreshed.setLayout(new MigLayout("wrap", "[grow,fill]"));
		
		JPanel canceledOrdersRefreshed = new JPanel();
		canceledOrdersRefreshed.setBackground(Color.WHITE);	
		canceledOrdersRefreshed.setLayout(new MigLayout("wrap", "[grow,fill]"));
		
		JPanel statisticsPanel = new JPanel();
		statisticsPanel.setBackground(Color.WHITE);	
		statisticsPanel.setLayout(new MigLayout("wrap", "[grow,fill]"));
		statisticsPanel.add(statistics(),"pushx,growx,pushy,growy");
		
		JPanel employeesPanel = new JPanel();
		employeesPanel.setBackground(Color.WHITE);	
		employeesPanel.setLayout(new MigLayout("wrap", "[grow,fill]"));
		employeesPanel.add(employees(),"pushx,growx,pushy,growy");
		
		/**
		 *Create Tabs
		 *See implemented Interfaces for each tabs(panels)
		 */
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frmBelvedereTowersCafe.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		tabbedPane.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent changeEvent) {
				int index = tabbedPane.getSelectedIndex();
				Component actOrders = null;
				Component comOrders = null;
				Component canOrders = null;
				
				try {
					actOrders = activeOrders();
					comOrders = completedOrders();
					canOrders = canceledOrders();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				if(tabbedPane.getTitleAt(index) == "Active Orders"){
					activeOrdersRefreshed.removeAll();
					activeOrdersRefreshed.add(actOrders,"pushx,growx,pushy,growy"); 
				}else if(tabbedPane.getTitleAt(index) == "Completed Orders"){
					completedOrderRefreshed.removeAll();
					completedOrderRefreshed.add(comOrders,"pushx,growx,pushy,growy"); 
				}else if(tabbedPane.getTitleAt(index) == "Canceled Orders"){
					canceledOrdersRefreshed.removeAll();
					canceledOrdersRefreshed.add(canOrders,"pushx,growx,pushy,growy"); 
				}
			}
		});
		
		tabbedPane.addTab("Order Menu", null, addOrders(), null);
		tabbedPane.addTab("Active Orders", null, activeOrdersRefreshed, null);
		tabbedPane.addTab("Completed Orders", null, completedOrderRefreshed, null);
		tabbedPane.addTab("Canceled Orders", null, canceledOrdersRefreshed, null);
		
	    loginMenuItem.addActionListener(new ActionListener() {
		   public void actionPerformed(ActionEvent e) {
			   final JFrame frame = new JFrame("");
	            LoginDialog loginDlg = new LoginDialog(frame);
	            loginDlg.setVisible(true);
	            // if logged on successfully
	            if(loginDlg.isSucceeded()){
	            	tabbedPane.addTab("Statistics", null, statisticsPanel, null);
	        		tabbedPane.addTab("Employees", null, employeesPanel, null);
	        		fileMenu.remove(loginMenuItem);
	        		fileMenu.add(logoutMenuItem);
	            }
		   }
	    });
	    
	    logoutMenuItem.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		fileMenu.remove(logoutMenuItem);	
	    		fileMenu.add(loginMenuItem);
	    		tabbedPane.remove(statisticsPanel);
	    		tabbedPane.remove(employeesPanel);
			}
	    });
	}
}