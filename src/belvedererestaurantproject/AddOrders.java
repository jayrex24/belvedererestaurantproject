package belvedererestaurantproject;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import net.miginfocom.swing.MigLayout;

/**
 @see Main.java
 @returns add_orders panel
 */
public interface AddOrders extends ActionListener{
	
	JTable table = new JTable();
	JPanel add_orders = new JPanel();
	
	MysqlConnect db = new MysqlConnect();
	TableBuilder tb   = new TableBuilder();
	
	String pattern = "###,###.###";
	DecimalFormat decimalFormat = new DecimalFormat(pattern);
	
	/** Chosen Menu Item array */
	ArrayList<Menu> list = new ArrayList<Menu>();

	/** Appetizer Buttons*/
	JToggleButton tglbtnChickenChaat 	= new JToggleButton("Chicken Chaat");
	JToggleButton tglbtnVeggieMomo 		= new JToggleButton("Veggie Momo");
	JToggleButton tglbtnChickenCholia 	= new JToggleButton("Chicken Cholia");
	JToggleButton tglbtnChanaChaat 		= new JToggleButton("Chana Chaat ");
	JToggleButton tglbtnChiliChicken 	= new JToggleButton("Chili Chicken");
	JToggleButton tglbtnPappadam 		= new JToggleButton("Pappadam");

	/** Main Dish Buttons */
	JToggleButton tglbtnChickenTikka 	= new JToggleButton("Chicken Tikka");
	JToggleButton tglbtnChickenSaag 	= new JToggleButton("Chicken Saag");
	JToggleButton tglbtnLambCurry 		= new JToggleButton("Lamb Curry");
	JToggleButton tglbtnLambKorma 		= new JToggleButton("Lamb Korma");
	JToggleButton tglbtnDalMakhani 		= new JToggleButton("Dal Makhani");
	JToggleButton tglbtnFishCurry 		= new JToggleButton("Fish Curry");
	JToggleButton tglbtnGarlicNaan 		= new JToggleButton("Garlic Naan");
	JToggleButton tglbtnShrimpVindaloo 	= new JToggleButton("Shrimp Vindaloo");
	JToggleButton tglbtnSteakSub 		= new JToggleButton("Steak Sub");
	JToggleButton tglbtnHamburger 		= new JToggleButton("Hamburger");
		
	/** Dessert Buttons */
	JToggleButton tglbtnCarrotCake 		= new JToggleButton("Carrot Cake");
	JToggleButton tglbtnChocolateCake 	= new JToggleButton("Chocolate Cake");
	JToggleButton tglbtnCheeseCake 		= new JToggleButton("Cheese Cake");

	/** Beverages Buttons */
	JToggleButton tglbtnMangoLassi 		= new JToggleButton("Mango Lassi");
	JToggleButton tglbtnLiterSoda 		= new JToggleButton("2 Liter Soda");
	JToggleButton tglbtnPlainLassi 		= new JToggleButton("Plain Lassi");
	JToggleButton tglbtnBottleOfSoda 	= new JToggleButton("Bottle of Soda");

	/** Continue Button */
	JButton btnContinue = new JButton("Continue");

	/** Return Buttons */
	JButton btnCancel = new JButton("Cancel Order");
	JButton btnGoBack = new JButton("Go Back");
	
	/** finish order button */
	JButton btnFinishOrder = new JButton("Finish Order");

	/** Add new order */
	JButton btnAddNewOrder = new JButton("Add New Order");
	
	/** warning label */
	JLabel warningOcc = new JLabel("All tables occupied!");

	default void actionPerformed(ActionEvent e) {
		if (tglbtnHamburger.isSelected()
			|| tglbtnSteakSub.isSelected() 
			|| tglbtnShrimpVindaloo.isSelected()
			|| tglbtnChiliChicken.isSelected() 
			|| tglbtnChanaChaat.isSelected() 
			|| tglbtnChickenCholia.isSelected()
			|| tglbtnVeggieMomo.isSelected() 
			|| tglbtnChickenChaat.isSelected() 
			|| tglbtnPappadam.isSelected()
			|| tglbtnGarlicNaan.isSelected() 
			|| tglbtnFishCurry.isSelected() 
			|| tglbtnChickenTikka.isSelected()
			|| tglbtnChickenSaag.isSelected() 
			|| tglbtnLambCurry.isSelected() 
			|| tglbtnLambKorma.isSelected()
			|| tglbtnDalMakhani.isSelected() 
			|| tglbtnCarrotCake.isSelected() 
			|| tglbtnChocolateCake.isSelected()
			|| tglbtnCheeseCake.isSelected() 
			|| tglbtnMangoLassi.isSelected() 
			|| tglbtnLiterSoda.isSelected()
			|| tglbtnPlainLassi.isSelected() 
			|| tglbtnBottleOfSoda.isSelected()) {
			btnContinue.setEnabled(true);
		} else {
			btnContinue.setEnabled(false);
		}
	}
	
	@SuppressWarnings("unchecked")
	default Component addOrders() throws SQLException, IOException{
		/** Establish Connection */
		MysqlConnect.getDbCon();
		
		/** Get items from MENU table - query */
		String sql = "SELECT * FROM MENU";
		
		/** pass sql string to be queried */
		ResultSet menuResult = db.query(sql);
		
		/** go to last result to get the row number */
		menuResult.last(); 
		
		/** Create item object array - add 1 so it doesn't go out of bounds */
		Menu[] item = new Menu[menuResult.getRow()+1];
		
		/** go back to the index so that we can loop through it */
		menuResult.beforeFirst();
		
		/** loop through result set */
		while (menuResult.next()) {
			/** Create dynamic menu items to be added later on to the table list */
			item[menuResult.getInt("item_no")] = new Menu(menuResult.getInt("item_no"), menuResult.getString("item_name"), menuResult.getDouble("price"));
		}
		
		URL getImage = Main.class.getResource("/resources/pizza.jpg");
		
		ImageIcon image = new ImageIcon(getImage);
		JLabel label = new JLabel("", image, JLabel.CENTER);
		JPanel pizzaPanel = new JPanel(new BorderLayout());
		pizzaPanel.setBackground(Color.WHITE);
		pizzaPanel.add( label, BorderLayout.CENTER );

		/** Initial Panel */
		JPanel initial_menu = new JPanel();
		initial_menu.setBackground(Color.WHITE);
		add_orders.add(initial_menu);
		add_orders.setBackground(SystemColor.textHighlightText);
		add_orders.setLayout(new CardLayout(0, 0));
		
		JLabel lblCreateNewOrder = new JLabel("Create New Order");
		lblCreateNewOrder.setBounds(420, 5, 220, 31);
		lblCreateNewOrder.setFont(new Font("Dialog", Font.BOLD, 22));
		
		/** Ordering Panel */
		JPanel ordering_menu = new JPanel();
		ordering_menu.setBackground(Color.WHITE);
		add_orders.add(ordering_menu);
		ordering_menu.setLayout(new MigLayout("wrap"));
		
		JLabel lblOrderingMenu = new JLabel("Ordering Menu");
		lblOrderingMenu.setFont(new Font("Dialog", Font.BOLD, 22));

		CardLayout cardLayout = (CardLayout) add_orders.getLayout();
		btnAddNewOrder.setToolTipText("Create a new order");
		btnAddNewOrder.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				cardLayout.show(add_orders, "ordering menu");
			}
		});
		
		initial_menu.setLayout(new MigLayout("wrap","[grow]"));
		initial_menu.add(lblCreateNewOrder,  "center, gaptop 15, wrap");
		initial_menu.add(pizzaPanel, "wrap, pushx, growx, pushy, growy");
		initial_menu.add(btnAddNewOrder,"pushx, growx");

		/** New Tabs Under Order */
		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);

		/** Set continue disabled - not clickable until menuitem selected */
		btnContinue.setEnabled(false);

		/** Cancel button */
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/** Set all buttons as not selected on cancel */
				cardLayout.show(add_orders, "initial menu");
				tglbtnHamburger.setSelected(false);
				tglbtnSteakSub.setSelected(false);
				tglbtnShrimpVindaloo.setSelected(false);
				tglbtnChiliChicken.setSelected(false);
				tglbtnChanaChaat.setSelected(false);
				tglbtnChickenCholia.setSelected(false);
				tglbtnVeggieMomo.setSelected(false);
				tglbtnChickenChaat.setSelected(false);
				tglbtnPappadam.setSelected(false);
				tglbtnGarlicNaan.setSelected(false);
				tglbtnFishCurry.setSelected(false);
				tglbtnChickenTikka.setSelected(false);
				tglbtnChickenSaag.setSelected(false);
				tglbtnLambCurry.setSelected(false);
				tglbtnLambKorma.setSelected(false);
				tglbtnDalMakhani.setSelected(false);
				tglbtnCarrotCake.setSelected(false);
				tglbtnChocolateCake.setSelected(false);
				tglbtnCheeseCake.setSelected(false);
				tglbtnMangoLassi.setSelected(false);
				tglbtnLiterSoda.setSelected(false);
				tglbtnPlainLassi.setSelected(false);
				tglbtnBottleOfSoda.setSelected(false);
				btnContinue.setEnabled(false);
			}
		});

		ordering_menu.add(lblOrderingMenu, "span, wrap, center");
		ordering_menu.add(tabbedPane_1, "span, wrap, center, pushx, pushy, growx, growy");
		ordering_menu.add(btnCancel, "center, split");
		ordering_menu.add(btnContinue, "center, split");
		
		/** Add 1-10 qty to array */
		JComboBox<Integer> qty = new JComboBox<Integer>();
		for(int i = 1; i <= 10; i++){
			qty.addItem(i);
		}
		
		JComboBox<Integer> table_no = new JComboBox<Integer>();

		btnContinue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/** Add to array when menu item is picked */
				if (tglbtnChiliChicken.isSelected()) {
					list.add(item[1]);
				}
				if (tglbtnChanaChaat.isSelected()) {
					list.add(item[2]);
				}
				if (tglbtnChickenCholia.isSelected()) {
					list.add(item[3]);
				}
				if (tglbtnVeggieMomo.isSelected()) {
					list.add(item[4]);
				}
				if (tglbtnChickenChaat.isSelected()) {
					list.add(item[5]);
				}
				if (tglbtnPappadam.isSelected()) {
					list.add(item[6]);
				}
				if (tglbtnHamburger.isSelected()) {
					list.add(item[7]);
				}
				if (tglbtnSteakSub.isSelected()) {
					list.add(item[8]);
				}
				if (tglbtnShrimpVindaloo.isSelected()) {
					list.add(item[9]);
				}
				if (tglbtnGarlicNaan.isSelected()) {
					list.add(item[10]);
				}
				if (tglbtnFishCurry.isSelected()) {
					list.add(item[11]);
				}
				if (tglbtnChickenTikka.isSelected()) {
					list.add(item[12]);
				}
				if (tglbtnChickenSaag.isSelected()) {
					list.add(item[13]);
				}
				if (tglbtnLambCurry.isSelected()) {
					list.add(item[14]);
				}
				if (tglbtnLambKorma.isSelected()) {
					list.add(item[15]);
				}
				if (tglbtnDalMakhani.isSelected()) {
					list.add(item[16]);
				}
				if (tglbtnCarrotCake.isSelected()) {
					list.add(item[17]);
				}
				if (tglbtnChocolateCake.isSelected()) {
					list.add(item[18]);
				}
				if (tglbtnCheeseCake.isSelected()) {
					list.add(item[19]);
				}
				if (tglbtnMangoLassi.isSelected()) {
					list.add(item[20]);
				}
				if (tglbtnLiterSoda.isSelected()) {
					list.add(item[21]);
				}
				if (tglbtnPlainLassi.isSelected()) {
					list.add(item[22]);
				}
				if (tglbtnBottleOfSoda.isSelected()) {
					list.add(item[23]);
				}
				
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.setRowCount(0);
				Object rowData[] = new Object[3];
				for (int i = 0; i < list.size(); i++) {
					rowData[0] = list.get(i).id;
					rowData[1] = list.get(i).itemname;
					rowData[2] = list.get(i).itemprice;
					model.addRow(rowData);
				}
				
				TableColumn qtyColumn = table.getColumnModel().getColumn(3);
				qtyColumn.setCellEditor(new DefaultCellEditor(qty));
				
				table_no.removeAllItems();
				for(int x = 1; x <= 9; x++){
					table_no.addItem(x);
				}
				
				String sql_chk_tables = "SELECT T.table_no "
		                + "FROM ORDERS O "
		           + "LEFT JOIN TABLES T "
		                  + "ON O.table_id = T.table_id "
		               + "WHERE is_active = 1 ";
				
				ResultSet rst = null;
				try {
					rst = db.query(sql_chk_tables);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
					
				try {
					while(rst.next()){
						table_no.removeItem(rst.getObject("T.table_no"));
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
				if(table_no.getItemCount() == 0){
					btnFinishOrder.setEnabled(false);
					warningOcc.setFont(new Font("Dialog", Font.BOLD, 17));
					warningOcc.setForeground(Color.RED);
					warningOcc.setVisible(true);
				}else{
					btnFinishOrder.setEnabled(true);
					warningOcc.setVisible(false);
				}
				
				cardLayout.show(add_orders, "submit order");
			}
		});

		/** Appetizers */
		JPanel appetizers = new JPanel();
		appetizers.setBackground(Color.WHITE);
		tabbedPane_1.addTab("Appetizers", null, appetizers, null);
		appetizers.setLayout(new MigLayout("wrap 2", "[grow,fill]"));
		
		tglbtnChickenChaat.setPreferredSize(new Dimension(120, 120));
		tglbtnVeggieMomo.setPreferredSize(new Dimension(120, 120));
		tglbtnChickenCholia.setPreferredSize(new Dimension(120, 120));
		tglbtnChanaChaat.setPreferredSize(new Dimension(120, 120));
		tglbtnChiliChicken.setPreferredSize(new Dimension(120, 120));
		tglbtnPappadam.setPreferredSize(new Dimension(120, 120));
		
		tglbtnChickenChaat.setToolTipText("Chicken pieces and cucumbers mixed with chutney.");
		tglbtnVeggieMomo.setToolTipText("Mixed vegetable dumplings.");
		tglbtnChickenCholia.setToolTipText("Grilled chicken marinated in garlic, ginger, lemon sauce and spices");
		tglbtnChanaChaat.setToolTipText("Chick peas, potatoes and tomatoes mixed with special chutney.");
		tglbtnChiliChicken.setToolTipText("Chicken pieces marinated in garlic, ketchup, ginger and lemon sauce. Sauteed with green peppers, onions and fresh chili.");
		tglbtnPappadam.setToolTipText("Lentil crackers.");

		appetizers.add(tglbtnChickenChaat);
		appetizers.add(tglbtnVeggieMomo);
		appetizers.add(tglbtnChickenCholia);
		appetizers.add(tglbtnChanaChaat);
		appetizers.add(tglbtnChiliChicken);
		appetizers.add(tglbtnPappadam);

		/** Main Dishes */
		JPanel main_dish = new JPanel();
		main_dish.setBackground(Color.WHITE);
		tabbedPane_1.addTab("Main Entrees", null, main_dish, null);
		main_dish.setLayout(new MigLayout("wrap 2", "[grow,fill]"));

		tglbtnChickenTikka.setPreferredSize(new Dimension(120, 120));
		tglbtnChickenSaag.setPreferredSize(new Dimension(120, 120));
		tglbtnLambCurry.setPreferredSize(new Dimension(120, 120));
		tglbtnLambKorma.setPreferredSize(new Dimension(120, 120));
		tglbtnDalMakhani.setPreferredSize(new Dimension(120, 120));
		tglbtnFishCurry.setPreferredSize(new Dimension(120, 120));
		tglbtnGarlicNaan.setPreferredSize(new Dimension(120, 120));
		tglbtnShrimpVindaloo.setPreferredSize(new Dimension(120, 120));
		tglbtnSteakSub.setPreferredSize(new Dimension(120, 120));
		tglbtnHamburger.setPreferredSize(new Dimension(120, 120));
		
		tglbtnChickenTikka.setToolTipText("Tender boneless chicken pieces marinated in yogurt and spices.");
		tglbtnChickenSaag.setToolTipText("Boneless chicken pieces cooked with spinach cream and spices.");
		tglbtnLambCurry.setToolTipText("Lamb cubes cooked with curry spices.");
		tglbtnLambKorma.setToolTipText("Mildly spiced lamb cubes flavored with coconut and simmered with yogurt.");
		tglbtnDalMakhani.setToolTipText("Lentils cooked with butter and mildly spiced.");
		tglbtnFishCurry.setToolTipText("Fish cooked with special curry sauce.");
		tglbtnGarlicNaan.setToolTipText("Leavened bread stuffed with garlic and baked with butter.");
		tglbtnShrimpVindaloo.setToolTipText("Shrimp and potato cooked with spicy sauce.");
		tglbtnSteakSub.setToolTipText("Steak on a long bun.");
		tglbtnHamburger.setToolTipText("Beef ground patty on a bun, with cheese");

		main_dish.add(tglbtnChickenTikka);
		main_dish.add(tglbtnChickenSaag);
		main_dish.add(tglbtnLambCurry);
		main_dish.add(tglbtnLambKorma);
		main_dish.add(tglbtnDalMakhani);
		main_dish.add(tglbtnFishCurry);
		main_dish.add(tglbtnGarlicNaan);
		main_dish.add(tglbtnShrimpVindaloo);
		main_dish.add(tglbtnSteakSub);
		main_dish.add(tglbtnHamburger);

		/** Desserts */
		JPanel desserts = new JPanel();
		desserts.setBackground(Color.WHITE);
		tabbedPane_1.addTab("Desserts", null, desserts, null);
		desserts.setLayout(new MigLayout("wrap 2", "[grow,fill]"));

		tglbtnCarrotCake.setPreferredSize(new Dimension(120, 120));
		tglbtnChocolateCake.setPreferredSize(new Dimension(120, 120));
		tglbtnCheeseCake.setPreferredSize(new Dimension(120, 120));

		desserts.add(tglbtnCarrotCake);
		desserts.add(tglbtnChocolateCake);
		desserts.add(tglbtnCheeseCake);

		/** Beverages */
		JPanel Beverage = new JPanel();
		Beverage.setBackground(Color.WHITE);
		tabbedPane_1.addTab("Beverage", null, Beverage, null);
		Beverage.setLayout(new MigLayout("wrap 2", "[grow,fill]"));

		tglbtnMangoLassi.setPreferredSize(new Dimension(120, 120));
		tglbtnLiterSoda.setPreferredSize(new Dimension(120, 120));
		tglbtnPlainLassi.setPreferredSize(new Dimension(120, 120));
		tglbtnBottleOfSoda.setPreferredSize(new Dimension(120, 120));

		Beverage.add(tglbtnMangoLassi);
		Beverage.add(tglbtnLiterSoda);
		Beverage.add(tglbtnPlainLassi);
		Beverage.add(tglbtnBottleOfSoda);

		/** Add all menuitems to Actionlistener */
		tglbtnHamburger.addActionListener(this);
		tglbtnSteakSub.addActionListener(this);
		tglbtnShrimpVindaloo.addActionListener(this);
		tglbtnChiliChicken.addActionListener(this);
		tglbtnChanaChaat.addActionListener(this);
		tglbtnChickenCholia.addActionListener(this);
		tglbtnVeggieMomo.addActionListener(this);
		tglbtnChickenChaat.addActionListener(this);
		tglbtnPappadam.addActionListener(this);
		tglbtnGarlicNaan.addActionListener(this);
		tglbtnFishCurry.addActionListener(this);
		tglbtnChickenTikka.addActionListener(this);
		tglbtnChickenSaag.addActionListener(this);
		tglbtnLambCurry.addActionListener(this);
		tglbtnLambKorma.addActionListener(this);
		tglbtnDalMakhani.addActionListener(this);
		tglbtnCarrotCake.addActionListener(this);
		tglbtnChocolateCake.addActionListener(this);
		tglbtnCheeseCake.addActionListener(this);
		tglbtnMangoLassi.addActionListener(this);
		tglbtnLiterSoda.addActionListener(this);
		tglbtnPlainLassi.addActionListener(this);
		tglbtnBottleOfSoda.addActionListener(this);
		
		/** Submit Panel */
		JPanel submit_order = new JPanel();
		submit_order.setBackground(Color.WHITE);
		submit_order.setLayout(new MigLayout("wrap"));
		add_orders.add(submit_order);
		
		JLabel lblSubmitOrder = new JLabel("Submit Order");
		lblSubmitOrder.setFont(new Font("Dialog", Font.BOLD, 22));
		
		JLabel lblNumberOfCustomer = new JLabel("No. of Customer(s)");
		lblNumberOfCustomer.setFont(new Font("Dialog", Font.BOLD, 11));
		
		JLabel lblEmployee = new JLabel("Employee: ");
		lblEmployee.setFont(new Font("Dialog", Font.BOLD, 11));
		
		JLabel lblReserved = new JLabel("Reserved: ");
		lblReserved.setFont(new Font("Dialog", Font.BOLD, 11));
		
		JCheckBox reservedBox = new JCheckBox();
		reservedBox.setSelected(false);
		
		JLabel lblPhoneNum = new JLabel("Telephone Number");
		lblPhoneNum.setFont(new Font("Dialog", Font.BOLD, 11));
		
		JTextField phoneNum = new JTextField();
		phoneNum.setText("");
		
		@SuppressWarnings("rawtypes")
		JComboBox curr_emp = new JComboBox();
		String sql_emp = "SELECT employee_id, CONCAT(fname,' ',lname) as `Full Name` "
				         + "FROM EMPLOYEE "
				        + "WHERE job_type = 'waiter' ";
		ResultSet employees = db.query(sql_emp);
        while(employees.next()){
            curr_emp.addItem(new ComboItem(employees.getObject("Full Name").toString(),employees.getObject("employee_id").toString()));
        }  
		
		JComboBox<Integer> no_customer = new JComboBox<Integer>();
		for(int x = 1; x <= 6; x++){
			no_customer.addItem(x);
		}

		JLabel lblTableNumber = new JLabel("Table Number");
		lblTableNumber.setFont(new Font("Dialog", Font.BOLD, 11));
				
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(table);

		table.setModel(new javax.swing.table.DefaultTableModel(
			new Object[][] {}, 
			new String[] { "Item ID", "Item Name", "Item Price", "Quantity"}
		));
		
		/** submit order page */
		btnGoBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				list.clear();
				cardLayout.show(add_orders, "ordering menu");
			}
		});
		
		/** Review Panel */
		JPanel review_order = new JPanel();
		review_order.setBackground(Color.WHITE);
		add_orders.add(review_order);
		review_order.setLayout(new MigLayout("wrap"));
		
		JLabel lblOrderReview = new JLabel("Order Review");
		lblOrderReview.setFont(new Font("Dialog", Font.BOLD, 22));
		
		JScrollPane scrollPane1 = new JScrollPane();		
		review_order.add(lblOrderReview,"wrap, center");
		
		JLabel lblTotal = new JLabel("Total:");
		lblTotal.setFont(new Font("Dialog", Font.BOLD, 11));
		JLabel txtTotal = new JLabel();
		
		JLabel lblOrder = new JLabel("Order ID:");
		lblOrder.setFont(new Font("Dialog", Font.BOLD, 11));
		JLabel txtOrder = new JLabel();
		
		JLabel lblTableNo = new JLabel("Table Number:");
		lblTableNo.setFont(new Font("Dialog", Font.BOLD, 11));
		JLabel txtTableNo = new JLabel();
		
		JLabel lblCustNo = new JLabel("No. of Customer(s):");
		lblCustNo.setFont(new Font("Dialog", Font.BOLD, 11));
		JLabel txtCustNo = new JLabel();
		
		/** 
		 * Once a customer has clicked finished - there is no going back,
		 * They will be able to edit however.
		 */
		
		btnFinishOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				LocalDateTime currDate = LocalDateTime.now();
				
				Object[][] orders = getTableData(table);
				int num_cust = (int) no_customer.getSelectedItem();
				int table_num = (int) table_no.getSelectedItem();
				int table_id = 0;
				int order_id = 0;
				int bill_id  = 0;
				
				/** Update Total later - this needs to be done in active orders page */
			    String sql_bill = "INSERT INTO BILL "
			    				+ "SET bill_id   = NULL, "
			    				    + "date_time = '"+currDate+"', "
			    				    + "total     = 0";
			    try {
					bill_id = db.set(sql_bill);
				} catch (SQLException e) {
					e.printStackTrace();
				}   
			    
			    Object emp = curr_emp.getSelectedItem();
			    String employee_id = ((ComboItem)emp).getValue();
				
			    String sql_tables = "INSERT INTO TABLES "
			    				  + "SET table_id    = NULL, "
			    				      + "table_no    = "+table_num+", "
			    				      + "employee_id = '"+employee_id+"' ";
			    
			    try {
					table_id = db.set(sql_tables);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			    int i = 1;
			    int cust_id  = 0;
			    int reservingCust = 0;
				while(i <= num_cust){	
					
			        String sql_cust = "INSERT INTO CUSTOMER "
			        				+ "SET cust_id     = NULL, "
			        					+ "c_fname     = NULL, "
			        					+ "c_lname     = NULL, "
			        					+ "address     = NULL, "
			        					+ "city 	   = NULL, "
			        					+ "state       = NULL, "
			        					+ "postal_code = NULL, "
			        					+ "phone_no    = NULL ";
				    try {
						cust_id = db.set(sql_cust);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				    
				    if(reservingCust == 0){
				    	reservingCust = cust_id;
				    }
				    
				    String sql_cust_table = "INSERT INTO CUST_TABLE "
				    					  + "SET table_id = "+table_id+", "
				    					  	  + "cust_id  = '"+cust_id+"' ";
				    try {
				    	db.set(sql_cust_table);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				       
				    i++;
				}
				
			    if(reservedBox.isSelected()){

					String sql_reserved = "INSERT INTO RESERVATION "
										 + "SET reservation_id = NULL, "
										     + "phone_no       = '"+phoneNum.getText()+"', "
										     + "date_time      = '"+currDate+"', "
										     + "cust_id        = '"+reservingCust+"'";
				    try {
				    	db.set(sql_reserved);
					} catch (SQLException e) {
						e.printStackTrace();
					}
			    }
				
				String sql_order = "INSERT INTO ORDERS "
								 + "SET order_id     = NULL, "
								     + "table_id     = '"+table_id+"', "
								     + "bill_id      = '"+bill_id+"', "
								     + "is_active    = 1, " // new order (flip active)
								     + "is_canceled  = 0 "; // order has just been place - not yet canceled
			    try {
					order_id = db.set(sql_order);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				for (Object[] order:orders){
					ArrayList<Object> order_list = new ArrayList<Object>();
					for(Object ord:order){
						order_list.add(ord);
					}
					
					int item_no = (int) order_list.get(0);
					String item_name = (String) order_list.get(1);
					double price = (double) order_list.get(2);
					Object quantity = order_list.get(3);
				
					if(quantity == null){
						quantity = 1;
					}
			       
				    String sql_tbl_order = "INSERT INTO TABLE_ORDERS "
				    					 + "SET order_id  = '"+order_id+"', "
				    					     + "item_no   = '"+item_no+"', "
				    					     + "item_name = '"+item_name+"', "
				    					     + "price     = '"+price+"', "
				    					     + "quantity  = '"+quantity+"' ";
				    try {
						db.set(sql_tbl_order);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
		
				String sql_curr_order = "SELECT item_no as `Item Number`, item_name as `Item Name`, quantity as `Quantity`, price as `Price`"
						               + " FROM TABLE_ORDERS T"
						          + " LEFT JOIN ORDERS O"
						                 + " ON T.order_id = O.order_id "
						           + "LEFT JOIN TABLES X"
						                 + " ON O.table_id = X.table_id"
						              + " WHERE O.order_id = "+ order_id +" ";
				
				double total_cost = 0;
				try {
					ResultSet rs1 = db.query(sql_curr_order);
					scrollPane1.setViewportView(tb.buildTable(rs1));
					
					rs1.beforeFirst();
					while(rs1.next()){
						total_cost += rs1.getDouble("price") * rs1.getDouble("quantity");
					}
					
					txtTotal.setText("$"+decimalFormat.format(total_cost));
					txtOrder.setText(order_id+"");
					txtTableNo.setText(table_num+"");
					txtCustNo.setText(num_cust+"");	
				
					
					rs1.close();	
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				no_customer.setSelectedIndex(0);
				table_no.setSelectedIndex(0);
				
				reservedBox.setSelected(false);
				phoneNum.setText("");
				
				cardLayout.show(add_orders, "review order");
			}
		});
		
		/** Review Order Page */
		JButton btnFinishReview = new JButton("Finish");
		btnFinishReview.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	        	list.clear();
	        	tglbtnHamburger.setSelected(false);
				tglbtnSteakSub.setSelected(false);
				tglbtnShrimpVindaloo.setSelected(false);
				tglbtnChiliChicken.setSelected(false);
				tglbtnChanaChaat.setSelected(false);
				tglbtnChickenCholia.setSelected(false);
				tglbtnVeggieMomo.setSelected(false);
				tglbtnChickenChaat.setSelected(false);
				tglbtnPappadam.setSelected(false);
				tglbtnGarlicNaan.setSelected(false);
				tglbtnFishCurry.setSelected(false);
				tglbtnChickenTikka.setSelected(false);
				tglbtnChickenSaag.setSelected(false);
				tglbtnLambCurry.setSelected(false);
				tglbtnLambKorma.setSelected(false);
				tglbtnDalMakhani.setSelected(false);
				tglbtnCarrotCake.setSelected(false);
				tglbtnChocolateCake.setSelected(false);
				tglbtnCheeseCake.setSelected(false);
				tglbtnMangoLassi.setSelected(false);
				tglbtnLiterSoda.setSelected(false);
				tglbtnPlainLassi.setSelected(false);
				tglbtnBottleOfSoda.setSelected(false);
				btnContinue.setEnabled(false);
	        	cardLayout.show(add_orders, "initial menu");
	        }
	    });
		
		submit_order.add(lblSubmitOrder,"wrap, center");
		submit_order.add(lblEmployee,"split 2, sg 1");
		submit_order.add(curr_emp,"wrap");
		submit_order.add(lblNumberOfCustomer, "split 2, sg 1");
		submit_order.add(no_customer);
		submit_order.add(lblTableNumber, "split 3,sg 1");
		submit_order.add(table_no);
		submit_order.add(warningOcc,"wrap");
		submit_order.add(lblReserved, "split 2");
		submit_order.add(reservedBox, "wrap");
		submit_order.add(lblPhoneNum, "split 2");
		submit_order.add(phoneNum,"sg 1");
		submit_order.add(scrollPane,"wrap, pushx, growx, pushy, growy");
		submit_order.add(btnGoBack,"center, sg 1, split");
		submit_order.add(btnFinishOrder, "center, sg 1, split");
	
		review_order.add(lblTotal,"split, sg 1");
		review_order.add(txtTotal,"wrap, sg 1");
		review_order.add(lblOrder,"split, sg 1");
		review_order.add(txtOrder,"wrap, sg 1");
		review_order.add(lblTableNo,"split, sg 1");
		review_order.add(txtTableNo,"wrap, sg 1");
		review_order.add(lblCustNo,"split, sg 1");
		review_order.add(txtCustNo,"wrap, sg 1");
		review_order.add(scrollPane1,"wrap, pushx, growx, pushy, growy");
		review_order.add(btnFinishReview,"center, sg 1, split");
		
		add_orders.add(initial_menu, "initial menu");
		add_orders.add(ordering_menu, "ordering menu");
		add_orders.add(submit_order, "submit order");
		add_orders.add(review_order, "review order");
	
		return add_orders;
	}
	
	/** Get all data from a table and place in array 2d*/
	default Object[][] getTableData (JTable table) {
	    TableModel dtm = table.getModel();
	    int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
	    Object[][] tableData = new Object[nRow][nCol];
	    for (int i = 0 ; i < nRow ; i++)
	        for (int x = 0 ; x < nCol ; x++)
	            tableData[i][x] = dtm.getValueAt(i,x);
	    return tableData;
	}
	
	class ComboItem {
	    private String key;
	    private String value;

	    public ComboItem(String key, String value){
	        this.key = key;
	        this.value = value;
	    }

	    @Override
	    public String toString(){
	        return key;
	    }

	    public String getKey(){
	        return key;
	    }

	    public String getValue(){
	        return value;
	    }
	}
}
