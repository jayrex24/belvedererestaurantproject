package belvedererestaurantproject;

/**
 @uses AddOrders.java
 @note Organizes menuItems
 */
public class Menu {
    public int id;
    public String itemname;
    public double itemprice;
    
    public Menu(int itemId, String itemName, double itemPrice) {
        this.id = itemId;
        this.itemname = itemName;
        this.itemprice = itemPrice;
    }
}
