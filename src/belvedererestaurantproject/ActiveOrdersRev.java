package belvedererestaurantproject;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;

public interface ActiveOrdersRev {
	
	JPanel new_active_orders = new JPanel();
	MysqlConnect db = new MysqlConnect();
	TableBuilder tb   = new TableBuilder();
	String pattern = "###,###.###";
	DecimalFormat decimalFormat = new DecimalFormat(pattern);
	
	default Component activeOrdersRev() throws SQLException {
		new_active_orders.removeAll();
		new_active_orders.setBackground(Color.WHITE);	
		new_active_orders.setLayout(new MigLayout("wrap 2", "[grow,fill]"));
	
		JLabel lblActiveOrders = new JLabel("Active Orders");
		lblActiveOrders.setFont(new Font("Dialog", Font.BOLD, 22));
		new_active_orders.add(lblActiveOrders, "wrap, center");
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new MigLayout("wrap 3"));
		new_active_orders.add(mainPanel, "pushx, pushy, growx, growy");
		
		String sql_curr_active = "SELECT order_id FROM ORDERS WHERE is_active = 1";
		ResultSet rs_curr_active = db.query(sql_curr_active);
		
		if (!rs_curr_active.isBeforeFirst() ) {    
			JLabel lblNoOrders = new JLabel("No Active Orders");
			lblNoOrders.setFont(new Font("Dialog", Font.BOLD, 30));
			lblNoOrders.setForeground (Color.red);
			mainPanel.add(lblNoOrders, "push, align center");
		} 
		
		ArrayList<Integer> active_array = new ArrayList<Integer>();
		while(rs_curr_active.next()){
			active_array.add(rs_curr_active.getInt("order_id"));
		}
		
		for(Integer ar:active_array){
			JButton editOrder = new JButton("Edit Order");
			JButton cancelOrder = new JButton("Cancel Order");
			JButton complete = new JButton("Finish Pay");
			JLabel lblTotal = new JLabel();
			JLabel lblOrderNo = new JLabel();
			JLabel lblTableNo = new JLabel();
			JPanel panel = new JPanel(new MigLayout("wrap"));
			
			JScrollPane scrollPane = new JScrollPane();
			String sql_active = "SELECT item_name as `Item Name`, quantity as `Quantity`, FORMAT(price*quantity,2) as `Total` "
					            + "FROM TABLE_ORDERS "
					           + "WHERE order_id = '"+ar+"' ";

			ResultSet rs = db.query(sql_active);
			rs.beforeFirst();
			
			double total = 0;
			while(rs.next()){
				total += rs.getDouble(3);
			}
			
			String sql_tbl = "SELECT table_no "
		                     + "FROM ORDERS O "
		                + "LEFT JOIN TABLES T "
		                       + "ON O.table_id = T.table_id "
		                    + "WHERE O.order_id = '"+ar+"' ";
			ResultSet rs1 = db.query(sql_tbl);
			
			int table_no = 0;
			while(rs1.next()){
				table_no = rs1.getInt("table_no");
			}
			
			lblTotal.setText("Total: $" +decimalFormat.format(total));
			lblOrderNo.setText("Order No: "+ar);
			lblTableNo.setText("Table No: "+table_no);
			scrollPane.setViewportView(tb.buildTable(rs));
			
			String allTotal = decimalFormat.format(total);
			
			cancelOrder.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					String sql_cancel = "UPDATE ORDERS O "
								   + "LEFT JOIN BILL B "
								   		  + "ON B.bill_id = O.bill_id "
							             + "SET B.total = "+allTotal+", "
							                 + "O.is_active = 0, "
							                 + "O.is_canceled = 1 "
							           + "WHERE O.order_id = "+ar+" ";
					
					String message = "Are you sure you want to cancel this order?";
					String title = "Cancel Order ("+ar+")";
					
				    int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
			        if (reply == JOptionPane.YES_OPTION) {
			        	try {
							db.set(sql_cancel);
							activeOrdersRev();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			        } else {
			        	JOptionPane.getRootFrame().dispose();  
			        }
				}	
			});
			
			editOrder.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					JCheckBox payCash = new JCheckBox("Pay Cash");
					JCheckBox payCredit = new JCheckBox("Pay Credit");
					String msg = "Selected payment type:"; 
					
					Object[] msgContent = {msg, payCash, payCredit}; 
					JOptionPane.showConfirmDialog ( null,  msgContent,  "Title", JOptionPane.YES_NO_OPTION); 
					boolean cash = payCash.isSelected();
					boolean credit = payCredit.isSelected();
					
					if(cash == true && credit == true){
						
						String sql_payCash = "INSERT INTO PAYMENT_TYPE "
										   + "SET p_type_id   = NULL, "
											   + "p_type_name = 'CASH', "
											   + "bill_id     = "+ar+" ";
						
						String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
								             + "SET p_type_id   = NULL, "
								                 + "p_type_name = 'CREDIT', "
								                 + "bill_id     = "+ar+" ";
						try {
							db.set(sql_payCash);
							db.set(sql_payCredit);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}	
						
					}else if(cash == true && credit != true){
						String sql_payCash = "INSERT INTO PAYMENT_TYPE "
								           + "SET p_type_id   = NULL, "
								               + "p_type_name = 'CASH', "
								               + "bill_id     = "+ar+" ";
						
						try {
							db.set(sql_payCash);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}	
						
					}else if(cash != false && credit == true){
						String sql_payCredit = "INSERT INTO PAYMENT_TYPE "
								             + "SET p_type_id   = NULL, "
								                 + "p_type_name = 'CREDIT', "
								                 + "bill_id     = "+ar+" ";
						try {
							db.set(sql_payCredit);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}	
					}		
				}	
			});
			
			complete.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					String sql_total = "UPDATE ORDERS O "
							      + "LEFT JOIN BILL B "
							             + "ON B.bill_id = O.bill_id "
							            + "SET B.total = "+allTotal+", "
							                + "O.is_active = 0 "
							          + "WHERE O.order_id = "+ar+" ";
					try {
						db.set(sql_total);
						activeOrdersRev();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}	
			});
			
			panel.add(scrollPane, "wrap");
			panel.add(lblOrderNo,"split, center, gapright 30");
			panel.add(lblTotal,"split, center, gapright 30");
			panel.add(lblTableNo,"wrap, center");
			panel.add(cancelOrder,"center, split");
			panel.add(editOrder,"center, split");
			panel.add(complete,"center");
			mainPanel.add(panel);
			
			rs.close();
			rs1.close();
		}

		new_active_orders.add(mainPanel,"pushx, growx, pushy, growy");
		
		return new_active_orders;
	}
}
