package belvedererestaurantproject;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import net.miginfocom.swing.MigLayout;

/**
 @see Main.java
 @returns statistics panel
 */
public interface Statistics {
	
	JPanel statistics = new JPanel(new MigLayout("wrap"));
	MysqlConnect db = new MysqlConnect();
	
	default Component statistics() throws SQLException {
		
		String[] pivots      = {"Select", "By Day", "By Month", "By Employee", "By Product"};	
		String[] datefilters = {"Total Customers", "Total Orders", "Total Revenue"};
		String[] empfilters  = {"Total Tables Served", "Total Customers Served"};
		String[] prodfilters = {"Top 5 Best Seller", "Top 5 Worst Seller"};
		
		
		statistics.setBackground(Color.WHITE);	
		statistics.setLayout(new MigLayout("wrap", "[grow]"));
	
		JLabel lblStatistics = new JLabel("Statistics");
		lblStatistics.setFont(new Font("Dialog", Font.BOLD, 22));
		
		JLabel lblPivot  = new JLabel("Pivot:");
		JLabel lblFilter = new JLabel("Filter:");
		JLabel lblFiller = new JLabel("");
		
		JComboBox<String> pivotDropdown = new JComboBox<String>();
		for(String p:pivots){
			pivotDropdown.addItem(p);
		}
	
		JComboBox<String> filterDropdown = new JComboBox<String>();
		filterDropdown.setEnabled(false);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.setEnabled(false);
		
		JScrollPane scrollPane = new JScrollPane();
		
		pivotDropdown.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				String selected = pivotDropdown.getSelectedItem().toString();
				
				switch(selected){
				case "By Day":
					filterDropdown.removeAllItems();
					filterDropdown.setEnabled(true);
					btnUpdate.setEnabled(true);
						
					for(String df:datefilters){
						filterDropdown.addItem(df);
					}
				
					btnUpdate.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {
							String secondSelected = filterDropdown.getSelectedItem().toString();
							
							switch(secondSelected){
							case "Total Customers":
								try {
									JFXPanel chart = makeLineChart(selected, secondSelected);
									scrollPane.setViewportView(chart);
								} catch (SQLException e1) {
									e1.printStackTrace();
								}   
								break;
							case "Total Orders":
								try {
									JFXPanel chart = makeLineChart(selected, secondSelected);
									scrollPane.setViewportView(chart);
								} catch (SQLException e1) {
									e1.printStackTrace();
								}  
								break;
							case "Total Revenue":
								try {
									JFXPanel chart = makeLineChart(selected, secondSelected);
									scrollPane.setViewportView(chart);
								} catch (SQLException e1) {
									e1.printStackTrace();
								}  
								break;
							default:
								break;
							}
						}
					});					
					
					break;
				case "By Month":
					filterDropdown.removeAllItems();
					filterDropdown.setEnabled(true);
					btnUpdate.setEnabled(true);
					
					for(String df:datefilters){
						filterDropdown.addItem(df);
					}
					
					btnUpdate.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {
							String secondSelected = filterDropdown.getSelectedItem().toString();
							
							switch(secondSelected){
							case "Total Customers":
								try {
									JFXPanel chart = makeBarChart(selected, secondSelected);
									scrollPane.setViewportView(chart);
								} catch (SQLException e1) {
									e1.printStackTrace();
								}   
								break;
							case "Total Orders":
								try {
									JFXPanel chart = makeBarChart(selected, secondSelected);
									scrollPane.setViewportView(chart);
								} catch (SQLException e1) {
									e1.printStackTrace();
								}  
								break;
							case "Total Revenue":
								try {
									JFXPanel chart = makeBarChart(selected, secondSelected);
									scrollPane.setViewportView(chart);
								} catch (SQLException e1) {
									e1.printStackTrace();
								}  
								break;
							default:
								break;
							}
						}
					});			
					
					break;
				case "By Employee":
					filterDropdown.removeAllItems();
					filterDropdown.setEnabled(true);
					btnUpdate.setEnabled(true);
					
					for(String ef:empfilters){
						filterDropdown.addItem(ef);
					}
					
					btnUpdate.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {
							String secondSelected = filterDropdown.getSelectedItem().toString();
							
							switch(secondSelected){
							case "Total Tables Served":
								try {
									JFXPanel chart = makeBarChart(selected, secondSelected);
									scrollPane.setViewportView(chart);
								} catch (SQLException e1) {
									e1.printStackTrace();
								}   
								break;
							case "Total Customers Served":
								try {
									JFXPanel chart = makeBarChart(selected, secondSelected);
									scrollPane.setViewportView(chart);
								} catch (SQLException e1) {
									e1.printStackTrace();
								}  
								break;
							default:
								break;
							}
						}
					});	

					break;
				case "By Product":
					filterDropdown.removeAllItems();
					filterDropdown.setEnabled(true);
					btnUpdate.setEnabled(true);
					
					for(String pf:prodfilters){
						filterDropdown.addItem(pf);
					}
					
					btnUpdate.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {
							String secondSelected = filterDropdown.getSelectedItem().toString();
							
							switch(secondSelected){
							case "Top 5 Best Seller":
								try {
									JFXPanel chart = makeBarChart(selected, secondSelected);
									scrollPane.setViewportView(chart);
								} catch (SQLException e1) {
									e1.printStackTrace();
								}   
								break;
							case "Top 5 Worst Seller":
								try {
									JFXPanel chart = makeBarChart(selected, secondSelected);
									scrollPane.setViewportView(chart);
								} catch (SQLException e1) {
									e1.printStackTrace();
								}  
								break;
							default:
								break;
							}
						}
					});	
					break;	
				default:
					filterDropdown.removeAllItems();
					btnUpdate.setEnabled(false);
					filterDropdown.setEnabled(false);
					break;
				}
			}
		});
		
		statistics.add(lblStatistics,"wrap, center");
		statistics.add(lblPivot,"split, sg");
		statistics.add(pivotDropdown,"wrap, sg 1");
		statistics.add(lblFilter,"split, sg");
		statistics.add(filterDropdown,"sg 1, wrap");
		statistics.add(lblFiller,"sg, split");
		statistics.add(btnUpdate," wrap");
		statistics.add(scrollPane,"pushx,growx,pushy,growy");
		
		return statistics;
	}
	
    	 
    @SuppressWarnings({ "unchecked", "rawtypes" })
	default JFXPanel makeLineChart(String pivot, String filter) throws SQLException {
    	
    	JFXPanel chartPanel = new JFXPanel();
        
        //defining the axes
        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel(pivot);
        yAxis.setLabel(filter);
        
        //creating the chart
        LineChart lineChart =  new LineChart(xAxis,yAxis);
        lineChart.setTitle(filter+" "+pivot);
        
        //defining a series
        XYChart.Series series = new XYChart.Series();
        series.setName(filter);
        
    	String query = pivot+" and "+filter;
  
    	switch (query) {
        case "By Day and Total Customers":
        	String sql = "SELECT COUNT(C.cust_id), DATE(B.date_time) "
        				 + "FROM BILL B "
        	        + "LEFT JOIN ORDERS O "
        	               + "ON O.bill_id = B.bill_id "
        	        + "LEFT JOIN CUST_TABLE CT "
        	               + "ON CT.table_id = O.table_id "
        	        + "LEFT JOIN CUSTOMER C "
        	               + "ON C.cust_id = CT.cust_id "
        	         + "GROUP BY DATE(B.date_time)";
        	ResultSet rs = db.query(sql);	
        	while(rs.next()){
        		series.getData().add(new XYChart.Data(rs.getDate("DATE(B.date_time)").toString(), rs.getInt("COUNT(C.cust_id)")));
        	}
        	rs.close();
            break;
        case "By Day and Total Orders":
        	String sql1 = "SELECT COUNT(O.order_id), DATE(B.date_time) "
		   				  + "FROM BILL B "
		   	         + "LEFT JOIN ORDERS O "
		   	                + "ON O.bill_id = B.bill_id "
		   	             + "WHERE O.is_canceled = 0 "
		   	               + "AND O.is_active = 0 "
		   	          + "GROUP BY DATE(B.date_time)";
		   	ResultSet rs1 = db.query(sql1);	
		   	while(rs1.next()){
		   		series.getData().add(new XYChart.Data(rs1.getDate("DATE(B.date_time)").toString(), rs1.getInt("COUNT(O.order_id)")));
		   	}
		   	rs1.close();
            break;
        case "By Day and Total Revenue": 
        	String sql2 = "SELECT sum(B.total), DATE(B.date_time) "
        			      + "FROM BILL B "
        			 + "LEFT JOIN ORDERS O "
        			        + "ON B.bill_id = O.bill_id "
        			     + "WHERE O.is_canceled = 0 "
        			       + "AND O.is_active = 0 "
        			  + "GROUP BY DATE(B.date_time)";
		   	ResultSet rs2 = db.query(sql2);	
		   	while(rs2.next()){
		   		series.getData().add(new XYChart.Data(rs2.getDate("DATE(B.date_time)").toString(), rs2.getBigDecimal("sum(B.total)")));
		   	}
		   	rs2.close();
            break;
        default:
        	break;
    	}
        
        Scene scene  = new Scene(lineChart);
        lineChart.getData().addAll(series);
        chartPanel.setScene(scene);
        
        return chartPanel;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	default JFXPanel makeBarChart(String pivot, String filter) throws SQLException {
    	
    	JFXPanel chartPanel = new JFXPanel();
        
        //defining the axes
        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel(pivot);
        yAxis.setLabel(filter);
        
        //creating the chart
        BarChart barChart =  new BarChart(xAxis,yAxis);
        barChart.setTitle(filter+" "+pivot);
        
        //defining a series
        XYChart.Series series = new XYChart.Series();
        series.setName(filter);
        
    	String query = pivot+" and "+filter;
  
    	switch (query) {
    	case "By Month and Total Customers":
        	String sql3 = "SELECT COUNT(C.cust_id), DATE_FORMAT(B.date_time,'%Y-%m') as `month`"
        			   	  + "FROM BILL B "
        	         + "LEFT JOIN ORDERS O "
        	                + "ON O.bill_id = B.bill_id "
        	         + "LEFT JOIN CUST_TABLE CT "
        	                + "ON CT.table_id = O.table_id "
        	         + "LEFT JOIN CUSTOMER C "
        	                + "ON C.cust_id = CT.cust_id "
        	          + "GROUP BY DATE_FORMAT(B.date_time,'%Y-%m')";
        	ResultSet rs3 = db.query(sql3);	
        	while(rs3.next()){
        		series.getData().add(new XYChart.Data(rs3.getObject("month").toString() + " ("+rs3.getInt("COUNT(C.cust_id)")+")", rs3.getInt("COUNT(C.cust_id)")));
        	}
        	rs3.close();
            break;
        case "By Month and Total Orders":
        	String sql4 = "SELECT COUNT(O.order_id), DATE_FORMAT(B.date_time,'%Y-%m') as `month`"
		   				  + "FROM BILL B "
		   	         + "LEFT JOIN ORDERS O "
		   	                + "ON O.bill_id = B.bill_id "
		   	             + "WHERE O.is_canceled = 0 "
		   	               + "AND O.is_active = 0 "
		   	          + "GROUP BY DATE_FORMAT(B.date_time,'%Y-%m')";
		   	ResultSet rs4 = db.query(sql4);	
		   	while(rs4.next()){
		   		series.getData().add(new XYChart.Data(rs4.getObject("month").toString() +" ("+rs4.getInt("COUNT(O.order_id)")+")", rs4.getInt("COUNT(O.order_id)")));
		   	}
		   	rs4.close();
            break;
        case "By Month and Total Revenue": 
        	String sql5 = "SELECT sum(B.total), DATE_FORMAT(B.date_time,'%Y-%m') as `month` "
        			      + "FROM BILL B "
        			 + "LEFT JOIN ORDERS O "
        			        + "ON B.bill_id = O.bill_id "
        			     + "WHERE O.is_canceled = 0 "
        			       + "AND O.is_active = 0 "
        			  + "GROUP BY DATE_FORMAT(date_time,'%Y-%m')";
		   	ResultSet rs5 = db.query(sql5);	
		   	while(rs5.next()){
		   		series.getData().add(new XYChart.Data(rs5.getObject("month").toString() +" ("+rs5.getBigDecimal("sum(B.total)")+")", rs5.getBigDecimal("sum(B.total)")));
		   	}
		   	rs5.close();
            break;
        case "By Employee and Total Tables Served":
        	String sql6 = "SELECT COUNT(*), CONCAT(E.fname, ' ', E.lname) as `Full Name` "
  				          + "FROM EMPLOYEE E "
  		             + "LEFT JOIN TABLES T "
  		                    + "ON T.employee_id = E.employee_id "
  		                 + "WHERE E.job_type = 'waiter' "
  		              + "GROUP BY CONCAT(E.fname, ' ', E.lname)";
		   	ResultSet rs6 = db.query(sql6);	
		   	while(rs6.next()){
		   		series.getData().add(new XYChart.Data(rs6.getObject("Full Name").toString() +" ("+rs6.getInt("COUNT(*)")+")", rs6.getInt("COUNT(*)")));
		   	}
		   	rs6.close();
            break;
        case "By Employee and Total Customers Served": 
        	String sql7 = "SELECT COUNT(*), CONCAT(E.fname, ' ', E.lname) as `Full Name` "
  			        	  + "FROM EMPLOYEE E "
  		             + "LEFT JOIN TABLES T "
  		                    + "ON T.employee_id = E.employee_id "
  		             + "LEFT JOIN CUST_TABLE CT "
  		                    + "ON CT.table_id = T.table_id "
  		                 + "WHERE E.job_type = 'waiter' "
  		              + "GROUP BY CONCAT(E.fname, ' ', E.lname)";
		   	ResultSet rs7 = db.query(sql7);	
		   	while(rs7.next()){
		   		series.getData().add(new XYChart.Data(rs7.getObject("Full Name").toString() + " ("+rs7.getInt("COUNT(*)")+")", rs7.getInt("COUNT(*)")));
		   	}
		   	rs7.close();
            break; 
        case "By Product and Top 5 Best Seller": 
        	String sql8 = "SELECT item_name AS `Item Name` , "
					           + "SUM(quantity) AS `Total Ordered` "
					      + "FROM TABLE_ORDERS "
					  + "GROUP BY item_no "
					  + "ORDER BY `Total Ordered` DESC "
					     + "LIMIT 5";
		   	ResultSet rs8 = db.query(sql8);	
		   	while(rs8.next()){
		   		series.getData().add(new XYChart.Data(rs8.getObject("Item Name").toString() + " ("+rs8.getInt("Total Ordered")+")", rs8.getInt("Total Ordered")));
		   	}
		   	rs8.close();
            break; 
        case "By Product and Top 5 Worst Seller": 
        	String sql9 = "SELECT item_name AS `Item Name` , "
					           + "SUM(quantity) AS `Total Ordered` "
					      + "FROM TABLE_ORDERS "
					  + "GROUP BY item_no "
					  + "ORDER BY `Total Ordered` ASC "
					     + "LIMIT 5";
		   	ResultSet rs9 = db.query(sql9);	
		   	while(rs9.next()){
		   		series.getData().add(new XYChart.Data(rs9.getObject("Item Name").toString() + " ("+rs9.getInt("Total Ordered")+")" , rs9.getInt("Total Ordered")));
		   	}
		   	rs9.close();
            break;     
        default:
        	break;
    	}
        
        Scene scene  = new Scene(barChart);
        barChart.getData().addAll(series);
        chartPanel.setScene(scene);
        
        return chartPanel;
    }
}
