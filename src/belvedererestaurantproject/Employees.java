package belvedererestaurantproject;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import net.miginfocom.swing.MigLayout;

/**
 @see Main.java
 @returns employees panel
 */
public interface Employees {

	JPanel employees = new JPanel();
	MysqlConnect db   = new MysqlConnect();
	TableBuilder tb   = new TableBuilder();

	default Component employees() throws SQLException {
		String[] shifts   = {"First", "Second"};
		String[] jobs     = {"Waiter", "Assistant Chef", "Chef", "Shift Manager", "Cleaning"};
		String[] salary   = {"20000", "25000", "30000", "35000", "40000", "45000", "50000", "55000", "60000"};
		
		employees.setBackground(Color.WHITE);
		employees.setLayout(new MigLayout("wrap", "[grow]"));

		JLabel lblemployees = new JLabel("Employees");
		lblemployees.setFont(new Font("Dialog", Font.BOLD, 22));
		
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel lblFilter   = new JLabel("Filter:");
		JLabel lblOperator = new JLabel("Operator:");
		JLabel lblSelect   = new JLabel("Select:");
		JLabel lblFiller   = new JLabel("");
		JButton btnSearch  = new JButton("Search");
		
		JComboBox<Object> select   = new JComboBox<Object>();
		
		JComboBox<Object> operator = new JComboBox<Object>();
		operator.addItem(">=");
		operator.addItem("<=");
		operator.addItem("=");

		JComboBox<String> view_by = new JComboBox<String>();
		view_by.addItem(null);
		view_by.addItem("By Shift");
		view_by.addItem("By Job Type");
		view_by.addItem("By Salary");
		
		btnSearch.setEnabled(false);
		select.setEnabled(false);
		operator.setEnabled(false);
			
		view_by.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				Object selected = view_by.getSelectedItem();
				
				 if(view_by.getSelectedIndex() == -1){
					 operator.setEnabled(false);
					 select.setEnabled(false);
					 select.removeAllItems();
					 btnSearch.setEnabled(false);
					 
					try {
						String sql = "SELECT * FROM EMPLOYEE";
					
						ResultSet rs = db.query(sql);
					
						JTable table_1 = tb.buildTable(rs);
						scrollPane.setViewportView(table_1);
					
						rs.close();
					
					} catch (SQLException se) {
						se.printStackTrace();
					}
					 
				 }else if (selected.toString().equals("By Shift")){
					operator.setEnabled(false);
					select.setEnabled(true);
					select.removeAllItems();
	
					for (String shift: shifts) {  
						select.addItem(shift);
					}
					
					select.setEnabled(true);
					btnSearch.setEnabled(true);
					
					btnSearch.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {
							String selectedShift = String.valueOf(select.getSelectedItem());
							
							try {
								String sql = "SELECT fname as `First Name`, "
										          + "lname as `Last Name`, "
										          + "phone_no as `Phone Number`, "
										          + "DOB as `Date Of Birth`, "
										          + "salary as `Salary`, "
										          + "shift_type as `Shift`, "
										          + "job_type as `Job`, "
										          + "supervisor_id as `Supervisor` "
										     + "FROM EMPLOYEE "
										    + "WHERE shift_type = '"+ selectedShift +"' ";
							
								ResultSet rs = db.query(sql);
						
								JTable table_1 = tb.buildTable(rs);
								scrollPane.setViewportView(table_1);
						
								rs.close();
						
							} catch (SQLException se) {
								se.printStackTrace();
							}
					
						}
					});

				 }else if (selected.toString().equals("By Job Type")){
					 operator.setEnabled(false);
					 select.removeAllItems();
						
					for (String job: jobs) {  
						select.addItem(job);
				    }
					
					select.setEnabled(true);
					btnSearch.setEnabled(true);
					
					btnSearch.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {
							String selectedJob = String.valueOf(select.getSelectedItem());	
							
							try {
								String sql = "SELECT fname as `First Name`, "
								                  + "lname as `Last Name`, "
								                  + "phone_no as `Phone Number`, "
								                  + "DOB as `Date Of Birth`, "
								                  + "salary as `Salary`, "
								                  + "shift_type as `Shift`, "
								                  + "job_type as `Job`, "
								                  + "supervisor_id as `Supervisor` "
								            + "FROM EMPLOYEE "
								           + "WHERE job_type = '"+ selectedJob +"' ";
								
								ResultSet rs = db.query(sql);

								JTable table = tb.buildTable(rs);
								scrollPane.setViewportView(table);

								rs.close();

							} catch (SQLException se) {
								se.printStackTrace();
							}
						}
					});
					
				 }else if (selected.toString().equals("By Salary")){
					select.removeAllItems();
					select.setEnabled(true);
					operator.setEnabled(true);
					btnSearch.setEnabled(true);
						
					for (String sal: salary) {  
						select.addItem(sal);
				    }
					
					btnSearch.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {
							String selectedSalary = String.valueOf(select.getSelectedItem());
							String selectedOper   = String.valueOf(operator.getSelectedItem());
							
							try {
								String sql = "SELECT fname as `First Name`, "
						                  		  + "lname as `Last Name`, "
						                  		  + "phone_no as `Phone Number`, "
						                  		  + "DOB as `Date Of Birth`, "
						                  		  + "salary as `Salary`, "
						                  		  + "shift_type as `Shift`, "
						                  		  + "job_type as `Job`, "
						                  		  + "supervisor_id as `Supervisor` "
						                     + "FROM EMPLOYEE "
						                    + "WHERE salary "+selectedOper+" '"+ selectedSalary +"' ";
								
								ResultSet rs = db.query(sql);
						
								JTable table_1 = tb.buildTable(rs);
								scrollPane.setViewportView(table_1);
						
								rs.close();
						
							} catch (SQLException se) {
								se.printStackTrace();
							}
					
						}
					});
				 }
			}
		});
		
		employees.add(lblemployees,"wrap, center");
		employees.add(lblFilter,"sg, split");
		employees.add(view_by,"sg 1, wrap");	
		employees.add(lblSelect, "sg, split");
		employees.add(select,"sg 1, wrap");
		employees.add(lblOperator,"sg, split");
		employees.add(operator,"sg 1, wrap");
		employees.add(lblFiller, "sg, split");
		employees.add(btnSearch,"wrap");
		employees.add(scrollPane,"pushx, growx, pushy, growy");
						
		return employees;
	}
}
