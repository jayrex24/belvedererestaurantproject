package belvedererestaurantproject;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import net.miginfocom.swing.MigLayout;

/**
 @see Main.java
 @returns completed_orders panel
 */
public interface CompletedOrders {
	JPanel completed_orders    = new JPanel();
	MysqlConnect db            = new MysqlConnect();
	TableBuilder tb   		   = new TableBuilder();
	JFrame detailFrame 		   = new JFrame();
	JScrollPane detScrollPanel = new JScrollPane();
	JButton exitButton 		   = new JButton("Exit");
	
	default Component completedOrders() throws SQLException {
		completed_orders.removeAll();
		detailFrame.revalidate();
		detailFrame.repaint();
		completed_orders.setBackground(Color.WHITE);
		completed_orders.setLayout(new MigLayout("wrap", "[grow]"));
		
		JLabel lblcompleted_orders = new JLabel("Completed Orders");
		lblcompleted_orders.setFont(new Font("Dialog", Font.BOLD, 22));
		
		JScrollPane scrollPane = new JScrollPane();
		
		String sql_completed = "SELECT O.order_id AS `Order ID`, COUNT(CT.cust_id) AS `No. of Customers`, IF(R.reservation_id IS NULL, 'No', 'Yes') AS `Reserved`, B.date_time AS `Datetime`, B.total AS `Total`, CONCAT(E.fname,' ', E.lname) AS `Server Name` "
				               + "FROM ORDERS O "
				          + "LEFT JOIN BILL B "
				                 + "ON O.bill_id = B.bill_id "
				          + "LEFT JOIN TABLES T "
				                 + "ON O.table_id = T.table_id "
				          + "LEFT JOIN EMPLOYEE E "
				                 + "ON T.employee_id = E.employee_id "
				          + "LEFT JOIN CUST_TABLE CT "
				                 + "ON T.table_id = CT.table_id "
				          + "LEFT JOIN RESERVATION R "
				                 + "ON R.cust_id = CT.cust_id " 
				              + "WHERE O.is_active = 0 "
				                + "AND O.is_canceled = 0 "
				           + "GROUP BY O.order_id "
				           + "ORDER BY O.order_id DESC";
				
		ResultSet rs = db.query(sql_completed);
					
		JTable table = tb.buildTable(rs);
		table.setPreferredScrollableViewportSize(table.getPreferredSize());
		scrollPane.setViewportView(table);
		
		detailFrame.setLayout(new MigLayout("wrap"));
		
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	        	
	            String order_id = table.getValueAt(table.getSelectedRow(), 0).toString();
	            
	            String sql_order_details = "SELECT item_no AS `Item No.`, item_name AS `Item Name`, price AS `Price`, quantity AS `Quantity` "
						                   + "FROM TABLE_ORDERS "
						                  + "WHERE order_id = '"+order_id+"' ";
	       
	            try {
					ResultSet rs_det = db.query(sql_order_details);
					
					JTable table_det = tb.buildTable(rs_det);
					
					detailFrame.setTitle("Order ("+order_id+") Details - Completed Order");
					detailFrame.setBounds(1100, 100, 500, 300);
					detailFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					
					detScrollPanel.setViewportView(table_det);
					
					detailFrame.add(detScrollPanel,"wrap, pushx, growx, pushy, growy");
					detailFrame.add(exitButton);
					detailFrame.setVisible(true);
			
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
	    });
		
		exitButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				detailFrame.setVisible(false);
			}	
		});
		
		rs.close();
	
		completed_orders.add(lblcompleted_orders,"wrap, center");
		completed_orders.add(scrollPane,"span, pushx, growx, pushy, growx");
	
		return completed_orders;
	}
}
