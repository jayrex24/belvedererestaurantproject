SET FOREIGN_KEY_CHECKS=0;
# Dump of table EMPLOYEE
# ------------------------------------------------------------
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `EMPLOYEE`;

CREATE TABLE `EMPLOYEE` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `minit` varchar(1) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(2) NOT NULL,
  `postal_code` int(5) NOT NULL,
  `phone_no` varchar(15) NOT NULL,
  `DOB` date NOT NULL,
  `salary` decimal(10,2) NOT NULL,
  `shift_type` enum('First','Second') NOT NULL,
  `job_type` varchar(30) NOT NULL,
  `supervisor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`employee_id`));

INSERT INTO `EMPLOYEE` (`employee_id`, `fname`, `lname`, `minit`, `address`, `city`, `state`, `postal_code`, `phone_no`, `DOB`, `salary`, `shift_type`, `job_type`, `supervisor_id`)
VALUES
  (1,'Thomas','Greene','Q','9605 Emerald Crossing','Parkertown','MD',21844,'747382746','1987-06-25',33000.00,'First','waiter',5),
  (2,'Fiona','Henderson','R','7747 Iron Falls','Stoneleigh Square','MD',21521,'738294758','1988-03-12',35000.00,'First','assistant chef',5),
  (3,'Yvonne','Johnston','E','8080 Foggy Nectar Range','Westgate','MD',21131,'234456789','1984-03-22',32000.00,'First','waiter',5),
  (4,'Dominic','Gray','E','1166 Misty Pines','Severnside','MD',20690,'234567897','1981-04-23',40000.00,'First','chef',5),
  (5,'Jan','Tucker','R','7515 Clear Heath','Sunset Gardens','MD',21026,'234567457','1981-05-13',32500.00,'First','waiter',5),
  (6,'Adrian','White','L','1166 Misty Pines','Severnside','MD',20690,'124672345','1983-01-12',45000.00,'First','shift manager',NULL),
  (7,'Sarah','Dickens','J','1007 Wishing Autumn Expressway','Villa Nova','MD',21629,'123574355','1984-02-11',25000.00,'First','cleaning',5),
  (8,'Ian','Martin','G','8453 Sleepy Apple Circuit','Martin','MD',21952,'234567214','1988-09-12',30000.00,'First','cleaning',5),
  (9,'Donna','Hardacre','B','3206 Merry View','Mil burn On The Magothy','MD',20887,'123564225','1990-01-08',46000.00,'Second','shift manager',NULL),
  (10,'Jennifer','Lewis','S','85 Silent Pointe','Parke West','MD',21674,'123453213','1986-06-10',28000.00,'First','cleaning',5),
  (11,'Wanda','Jackson','T','7515 Clear Heath','Sunset Gardens','MD',21026,'234523452','1981-07-06',35000.00,'Second','waiter',9),
  (12,'Diane','Edmunds','R','3408 Round Barn Hill','Mount Augustine','MD',20824,'543256462','1978-03-05',34500.00,'Second','cleaning',9),
  (13,'Sue','Underwood','D','2565 Burning Forest','Bestgate','MD',21516,'148532452','1974-04-15',36000.00,'Second','waiter',9),
  (14,'Kevin','Mackenzie','D','4399 Little Vale','Feesersburg','MD',21725,'457894533','1959-04-13',30000.00,'Second','waiter',9),
  (15,'Keith','North','G','5109 Colonial Promenade','Sandy Spring','MD',21304,'234673254','1991-02-19',34000.00,'Second','assistant chef',9),
  (16,'Elizabeth','Sharp','Q','101 Middle Lagoon Gardens','Antietam Manor','MD',21316,'234164221','1988-06-21',40000.00,'Second','chef',9),
  (17,'Austin','Hudson','T','3206 Merry View','Milburn On The Magothy','MD',20887,'123532341','1993-02-28',30000.00,'Second','waiter',9);


# Dump of table MENU
# ------------------------------------------------------------
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `MENU`;

CREATE TABLE `MENU` (
  `item_no` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(30) NOT NULL,
  `price` decimal(4,2) NOT NULL,
  `item_type` enum('Appetizer','Entree','Dessert','Beverage') NOT NULL,
  PRIMARY KEY (`item_no`));


INSERT INTO `MENU` (`item_no`, `item_name`, `price`, `item_type`)
VALUES
  (1,'Chili Chicken',5.99,'Appetizer'),
  (2,'Chana Chaat',6.99,'Appetizer'),
  (3,'Chicken Cholia',5.99,'Appetizer'),
  (4,'Veggie Momo',4.99,'Appetizer'),
  (5,'Chicken Chaat',4.99,'Appetizer'),
  (6,'Pappadam',4.99,'Appetizer'),
  (7,'Hamburger',7.99,'Entree'),
  (8,'Steak Sub',8.99,'Entree'),
  (9,'Shrimp Vindaloo',10.99,'Entree'),
  (10,'Garlic Naan',9.99,'Entree'),
  (11,'Fish Curry',8.99,'Entree'),
  (12,'Chicken Tikka',10.99,'Entree'),
  (13,'Chicken Saag',9.99,'Entree'),
  (14,'Lamb Curry',11.99,'Entree'),
  (15,'Lamb Korma',10.99,'Entree'),
  (16,'Dal Makhani',9.99,'Entree'),
  (17,'Carrot Cake',4.99,'Dessert'),
  (18,'Chocolate Cake',4.99,'Dessert'),
  (19,'Cheese Cake',5.99,'Dessert'),
  (20,'Mango Lassi',2.59,'Beverage'),
  (21,'2 Liter Soda',2.99,'Beverage'),
  (22,'Plain Lassi',1.99,'Beverage'),
  (23,'Bottle of Soda',1.49,'Beverage');

# Dump of table BILL
# ------------------------------------------------------------

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `BILL`;

CREATE TABLE `BILL` (
  `bill_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_time` datetime NOT NULL,
  `total` decimal(6,2) NOT NULL,
  PRIMARY KEY (`bill_id`));


# Dump of table TABLES
# ------------------------------------------------------------
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `TABLES`;

CREATE TABLE `TABLES` (
  `table_id` int(11) NOT NULL AUTO_INCREMENT,
  `table_no` int(1) NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`table_id`),
  FOREIGN KEY (`employee_id`) REFERENCES `EMPLOYEE` (`employee_id`));


# Dump of table ORDERS
# ------------------------------------------------------------
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `ORDERS`;

CREATE TABLE `ORDERS` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `table_id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0' COMMENT '0: not active, 1:active - default is not active',
  `is_canceled` int(1) NOT NULL DEFAULT '0' COMMENT '0: not canceled, 1:canceled - default is not canceled',
  PRIMARY KEY (`order_id`),
  FOREIGN KEY (`table_id`) REFERENCES `TABLES` (`table_id`),
  FOREIGN KEY (`bill_id`) REFERENCES `BILL` (`bill_id`));
 
# Dump of table CUSTOMER
# ------------------------------------------------------------
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `CUSTOMER`;

CREATE TABLE `CUSTOMER` (
  `cust_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_fname` varchar(50) DEFAULT '',
  `c_lname` varchar(50) DEFAULT '',
  `address` varchar(50) DEFAULT '',
  `city` varchar(50) DEFAULT '',
  `state` varchar(2) DEFAULT '',
  `postal_code` int(20) DEFAULT NULL,
  `phone_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`cust_id`));

# Dump of table PAYMENT_TYPE
# ------------------------------------------------------------
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `PAYMENT_TYPE`;

CREATE TABLE `PAYMENT_TYPE` (
  `p_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_type_name` enum('CARD','CASH') NOT NULL,
  `bill_id` int(11) NOT NULL,
  PRIMARY KEY (`p_type_id`),
  FOREIGN KEY (`bill_id`) REFERENCES `BILL` (`bill_id`));

# Dump of table RESERVATION
# ------------------------------------------------------------
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `RESERVATION`;

CREATE TABLE `RESERVATION` (
  `reservation_id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_no` varchar(20) NOT NULL,
  `date_time` datetime NOT NULL,
  `cust_id` int(11) NOT NULL,
  PRIMARY KEY (`reservation_id`),
  FOREIGN KEY (`cust_id`) REFERENCES `CUSTOMER` (`cust_id`));


# Dump of table CUST_TABLE
# ------------------------------------------------------------
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `CUST_TABLE`;

CREATE TABLE `CUST_TABLE` (
  `table_id` int(11) NULL,
  `cust_id` int(11) NULL,
  FOREIGN KEY (`cust_id`) REFERENCES `CUSTOMER` (`cust_id`),
  FOREIGN KEY (`table_id`) REFERENCES `TABLES` (`table_id`));

# Dump of table TABLE_ORDERS
# ------------------------------------------------------------
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `TABLE_ORDERS`;

CREATE TABLE `TABLE_ORDERS` (
  `order_id` int(11) NULL,
  `item_no` int(11) NULL,
  `item_name` varchar(50) NULL DEFAULT '',
  `price` float NULL,
  `quantity` int(11) NULL DEFAULT '1',
  FOREIGN KEY (`item_no`) REFERENCES `MENU` (`item_no`),
  FOREIGN KEY (`order_id`) REFERENCES `ORDERS` (`order_id`));



